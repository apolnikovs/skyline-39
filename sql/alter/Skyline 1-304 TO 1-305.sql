# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.304');

# ---------------------------------------------------------------------- #
# Vykintas Rutkunas Changes 											 #
# ---------------------------------------------------------------------- # 
INSERT INTO email (EmailCode, Title, Message) VALUES ('automated_report', 'Skyline Report', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"&gt; <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<style>

    body {
        font-family: Arial;
    }

    h1 {
        color: \#1F497D;
        font-size: 22px;
    }

    .left {
        display: inline-block;
        width: 150px;
    }

    .right {
        display: inline-block;
    }

    p {
        margin:0 0 2px 0;
        color: \#1F497D;
    }

</style>

</head>

<body>

<h1>Skyline Automated Report</h1>

<p>
    <span class="left">
        Created Date:
    </span>
    <span class="right">
        [date]
    </span>
</p>

<p>
    <span class="left">
        Author:
    </span>
    <span class="right">
        [createdBy]
    </span>
</p>

<p>
    <span class="left">
        Report Name:
    </span>
    <span class="right">
        [reportName]
    </span>
</p>

<p>
    <span class="left">
        Description:
    </span>
    <span class="right">
        [reportDescription]
    </span>
</p>

<br/>
<br/>

<p>Note: This report has been generated for your benefit. If you do not require this report of if it is not delivering the information you require please contact the Author immediately.<p>

<div style="margin-top:50px; font-size:12px;">
    <span style="text-decoration:underline; font-weight:bold;">Important Data Protection Information</span>
    <br/>
    <span style="display:block; max-width:600px;">
        This email (including any attachments) is private and confidential, and may be privileged.  It is for the
        exclusive use of the intended recipient(s).  If you have received this email in error, please inform the
        sender immediately and then delete this email.  Unless you have been given specific permission to
        do so, please do not distribute or copy this email or its contents.
    </span>
</div>

</body> </html>');

CREATE TABLE report_log ( ReportLogID INT(11) NOT NULL AUTO_INCREMENT, ReportID INT(11) NOT NULL, UserID INT(11) NOT NULL, UserFullName VARCHAR(100) NULL, ReportName VARCHAR(100) NULL DEFAULT NULL, ReportDescription VARCHAR(100) NULL DEFAULT NULL, Recipients VARCHAR(100) NULL DEFAULT NULL, Date DATETIME NOT NULL, PRIMARY KEY (ReportLogID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Vempati Srinivas Changes 	    										 #
# ---------------------------------------------------------------------- # 
REPLACE INTO `questionnaire_attribute` (`QuestionnaireAttributeID`, `AttributeName`) VALUES
(9, 'comments'),
(7, 'communication'),
(4, 'ease_of_booking'),
(8, 'engineer'),
(1, 'key_question_brand'),
(2, 'key_question_manufacturer'),
(3, 'key_question_service_agent'),
(11, 'permission_to_use_comments'),
(5, 'speed_of_service_first_visit'),
(6, 'speed_of_service_overall'),
(10, 'Who_suggested_our_services_to_you');



UPDATE `email` SET `Message` = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[PAGE_TITLE]</title>
</head>

<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:''Calibri'';">
	<tbody><tr>
		<td bgcolor="#ffffff" align="center">
        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:''Calibri''">
            	<tbody>
                
                <!-- #HEADER
        		================================================== -->
            	<tr>
                	<td width="640"><!-- End top bar table -->
                    </td>
                </tr>
                <tr>
                <td width="640" bgcolor="#52aae0" align="center">
    
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:''Calibri''" height="60px" colspan="2" valign="bottom">CUSTOMER RECEIPT</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:''Calibri''" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:''Calibri''">Email us: <a style="color:#ffffff;font-family:''Calibri''" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a></p></td>
        </tr>
        
    </tbody></table>
    
    <!-- End header -->
</td>
                </tr>
                <!-- #CONTENT
        		================================================== -->
                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>
                <tr>
                <td width="640" bgcolor="#ffffff">
    <table width="640" cellspacing="6" cellpadding="6" border="0">
        <tbody><tr>
            <td width="30"></td>
            <td width="580">
                <!-- #TEXT ONLY
                ================================================== -->
                <table width="580" cellspacing="6" cellpadding="6" border="0">
                    <tbody><tr>
                        <td width="405">
                            <p style="font-size:13px;font-family:''Calibri'';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>
                            
							
				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:''Calibri'';">Following the recent service we supplied we sincerely hope that we satisfied your requirement and that the standard of service exceeded your expectations.  Should you experience any issues with this product or any other similar products, please feel free to contact us to arrange a new service request.</p>
                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:''Calibri''"></p>
                                

                        </td>
                        <td width="175">
						                        
                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />
                       
					    </td>
                    </tr>
                   
                </tbody></table>
                <table>
                <tbody>
                
                    <tr><td width="580" height="2" style="background:#3576bc;font-family:''Calibri''"></td></tr>
                   
                    </tbody>
                    </table>
                                        
                <!-- #TEXT WITH ADDRESS
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="2" border="0">
                    <tbody>
					
                    <tr>
                        <td colspan="2" style="padding-bottom:25px;text-align:center;"  ><br>
                          <a href="[SERVICE_QUESTIONNAIRE]" style="color:#3576bc;font-size:16px;font-weight:bold;" >Please take a few minutes to complete our Service Questionnaire</a>
                       </td>
                        
                    </tr>
					
					
			    </tbody></table>
                                        
           
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr><td width="580" height="3" style="background:#3576bc;font-family:''Calibri''"></td></tr>
                </tbody></table>
                                        
                <!-- #TEXT FOOTER
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="100">
                    

<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:''Calibri''"></p>
<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:''Calibri''">Assuring you of our best intention at all times.</p>

<p style="font-size:13px;color:#444444;line-height:115%;font-family:''Calibri''">Yours Sincerely, <br />[BRAND_NAME] Customer Service Team</p>
                    </td>
                    </tr>
                </tbody></table>
                                                                                
                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY
                ================================================== -->
               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td valign="top">
                          
                        </td>
                       
                    </tr>
                </tbody></table>-->
                                        
                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE
                ================================================== -->
                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td width="180" valign="top">
                          
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                       
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                            
                        </td>
                    </tr>
                </tbody></table>-->
            </td>
            <td width="30"></td>
        </tr>
    </tbody></table>
</td>
                </tr>
                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->
                <!-- #FOOTER
   				 ================================================== -->
                <tr>
                <td width="640">
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:''Calibri''">
 <p style="font-size:13px;color:#ffffff; text-align:center;font-family:''Calibri''">If you have any queries contact us on <a style="color:#ffffff;font-family:''Calibri''" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>
            </td>
            </tr>

    </tbody></table>
</td>
                </tr>
               
            </tbody></table>
        </td>
	</tr>
</tbody></table>
</body>
</html>' WHERE `email`.`EmailID` = 16;

INSERT INTO `permission` (
`PermissionID` ,
`Name` ,
`Description` ,
`CreatedDate` ,
`EndDate` ,
`Status` ,
`ModifiedUserID` ,
`ModifiedDate` ,
`URLSegment`
)
VALUES (
'11011', 'Service Provider - Keynote Time Line', 'Display the Service Provider timeline on the open jobs page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', '1',
CURRENT_TIMESTAMP , NULL
), 

(
'11012', 'Branch - Open Jobs Timeline', 'Display the Branch Open Jobs timeline on the open jobs page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', '1',
CURRENT_TIMESTAMP , NULL
),
(
'11013', 'Job Update Page – Edit Open Jobs Status', 'Allow the user to edit the Open job status on the job update page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'In-active', '1',
CURRENT_TIMESTAMP , NULL
);


UPDATE `permission` SET `Name` = 'Job Update Page – Edit Job System Status' WHERE `permission`.`PermissionID` =12001;

INSERT INTO `group_headings` (
`GroupID` ,
`GroupHeading` ,
`Tooltip` ,
`Description` ,
`CreatedDate` ,
`EndDate` ,
`Status` ,
`ModifiedUserID` ,
`ModifiedDate` ,
`Code` ,
`ParentID` ,
`URLSegment`
)
VALUES (
NULL , 'Open Job Status Permissions', 'Open Job Status Permissions', 'This menu item allows specific Open Jobs Status''s to be assigned to users so that they can be selected on the Job update page.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL , '2013-06-28 11:18:00', 'openJobsStatusPermissions', '2', '/LookupTables/openJobsStatusPermissions'
);


CREATE TABLE IF NOT EXISTS `open_jobs_status` (
  `OpenJobsStatusCode` varchar(100) NOT NULL,
  `OpenJobsStatusName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `open_jobs_status` (`OpenJobsStatusCode`, `OpenJobsStatusName`) VALUES
('branch_repair', 'Branch Repair'),
('awaiting_parts', 'Awaiting Parts'),
('in_store', 'Pending Collection'),
('with_supplier', 'In Progress with Service Provider'),
('awaiting_collection', 'Returned to Branch'),
('customer_notified', 'Customer Notified'),
('Returned to Customer', 'closed');


CREATE TABLE IF NOT EXISTS `open_jobs_status_permission` (
  `OpenJobsStatusPermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `PermissionType` enum('user','role','network','client','brand','branch') NOT NULL,
  `EntityID` int(11) NOT NULL,
  `OpenJobsStatusCode` enum('branch_repair','awaiting_parts','in_store','with_supplier','awaiting_collection','customer_notified','closed') NOT NULL,
  PRIMARY KEY (`OpenJobsStatusPermissionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `job` CHANGE `OpenJobStatus` `OpenJobStatus` ENUM( 'branch_repair', 'awaiting_parts', 'in_store', 'with_supplier', 'awaiting_collection', 'customer_notified', 'closed' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.305');



