{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Clients}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
    var vat_rate_array = [];
    {foreach $countries as $country}
        vat_rate_array[{$country.CountryID}] = [];
        {if isset($vatRates[$country.CountryID])}            
            {foreach $vatRates[$country.CountryID] as $vatRateItem}
                vat_rate_array[{$country.CountryID}][{$vatRateItem.VatRateID}] = '{$vatRateItem.Code}';
            {/foreach}
        {/if}
    {/foreach}                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    function inactiveRow(nRow, aData)
    {
          
        if (aData[6]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(5)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
                  $('td:eq(5)', nRow).html( $statuses[0][0] );
        }
    }
    
   
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                if($("#nId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/OrganisationSetup/clients/'+urlencode($("#nId").val()));
                }
            }
        });



                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/organisationSetup');

                                });


                  
                  //Client compnay address lookup starts here..
                  $(document).on('click', '#quick_find_btn', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#PostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput").slideDown("slow");
                                           

                                            $("#BuildingNameNumber").val('');	
                                            $("#Street").val('');
                                            $("#LocalArea").val('');
                                            $("#TownCity").val('');

                                            $("#PostalCode").focus();      
                                    }
                                    else
                                    {

                                        $('#selectOutput').html(data); 
                                        $("#selectOutput").slideDown("slow");
                                        
                                         $("#postSelect").focus();

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')");
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                    
                    
                });                 
                //Client compnay address lookup ends here..
                
                
                
                
                  //Invoiced compnay address lookup starts here..
                  $(document).on('click', '#quick_find_btn2', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#InvoicedPostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput2').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput2").slideDown("slow");
                                           

                                            $("#InvoicedBuilding").val('');	
                                            $("#InvoicedStreet").val('');
                                            $("#InvoicedArea").val('');
                                            $("#InvoicedTownCity").val('');

                                            $("#InvoicedPostalCode").focus(); 
                                    }
                                    else
                                    {

                                        $('#selectOutput2').html(data); 
                                        $("#selectOutput2").slideDown("slow");
                                        
                                        $("#postSelect_2").focus(); 

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    //setTimeout("$('#quickButton').fadeIn('medium')");
                            },
                            url:'{$_subdomain}/index/addresslookup/2' 
                        } ); 
                    return false;
                    
                    
                });                 
                //Invoiced compnay address lookup ends here..
                
                
                
                


                 /* Add a change handler to the county dropdown - strats here*/
                /*$(document).on('change', '#CountyID', 
                    function() {
                         
                         
                         $("#CountyID option:selected").each(function () {
                            $selected_cc_id =  $(this).attr('id');
                            $country_id_array = $selected_cc_id.split('_');
                            if($country_id_array[1]!='0')
                            {
                                $("#CountryID").val($country_id_array[1]);
                                $("#CountryID").blur();
                                $("#CountryID").attr("disabled","disabled").addClass('disabledField');
                            }
                            else
                            {
                                 $("#CountryID").val('');
                                 $("#CountryID").removeAttr("disabled").removeClass('disabledField');
                            }
                            
                            
                        });

                        
                        
                    }      
                );*/
              /* Add a change handler to the county dropdown - ends here*/
              
              
             /***************************************
             * Change handler for Country drop down *
             ***************************************/
             /*$(document).on('change', '#CountryID', 
                    function() {
                        var options = '';
                        var selectedCountryRates = vat_rate_array[$('#CountryID').val()];
                        
                        $('#VATRateID').empty()

                        $('#VATRateID').append('<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>');
                             
                        for (var n = 0; n < selectedCountryRates.length; n++) {
                            if (selectedCountryRates[n] !== undefined) {
                            options += '<option value="' + n + '">' + selectedCountryRates[n] + '</option>';
                            }
                        }
                        
                        $('#VATRateID').append(options);
                    }      
                );*/             


                 /* Add a change handler to the network dropdown - strats here*/
                /*$(document).on('change', '#nId', 
                    function() {
                         
                        $(location).attr('href', '{$_subdomain}/OrganisationSetup/clients/'+urlencode($("#nId").val())); 
                    }      
                );*/
              /* Add a change handler to the network dropdown - ends here*/
              


               
                       



                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UA";
                     
                     {else}

                         var displayButtons =  "U";

                     {/if} 
                     


                    
                    $('#ClientsResults').PCCSDataTable( {
                            "aoColumns": [ 
                                                        /* ClientID */  {  "bVisible":    false },    
                                                        /* ClientName */   null,
                                                        /* NetworkID */  null,
                                                        /* AccountNumber */  null,
                                                        /* InvoiceNetwork */  null,
                                                        /* ServiceProviderID */  null,
                                                        /* Status */  null
                                                ],
                            "aaSorting": [[ 1, "asc" ]],
                            
                            
                            
                            
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/OrganisationSetup/clients/insert/',
                            createDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/Clients/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                                    
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    ClientName:
                                                        {
                                                            required: true
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: true
                                                        },
                                                    Street:
                                                        {
                                                            required: true
                                                        },
                                                    TownCity:
                                                        {
                                                            required: true
                                                        },
                                                    CountryID:
                                                        {
                                                            required: true
                                                        },
                                                    ContactEmail:
                                                        {
                                                            required: true,
                                                            email:true
                                                        }, 
                                                    ContactPhone:
                                                        {
                                                            required: true
                                                        },     
                                                    AccountNumber:
                                                        {
                                                            required: true
                                                        },  
                                                        
                                                    PaymentDiscount:
                                                        {
                                                            number: true
                                                        },  
                                                    PaymentTerms:
                                                        {
                                                            digits: true
                                                        },
                                                    SageNominalCode:
                                                        {
                                                            digits: true
                                                        },
						    DefaultTurnaroundTime: { 
							number:	    true,
							maxlength:  2
						    },
                                                    ForceReferralNumber:
                                                    {
                                                        required: true
                                                    }
                                                    
                                                        
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    NetworkID:
                                                        {
                                                            required: "{$page['Errors']['service_network']|escape:'html'}"
                                                        },
                                                    ClientName:
                                                        {
                                                            required: "{$page['Errors']['company_name']|escape:'html'}"
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: "{$page['Errors']['post_code']|escape:'html'}"
                                                        },
                                                    Street:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"
                                                        },
                                                    TownCity:
                                                        {
                                                            required: "{$page['Errors']['town']|escape:'html'}"
                                                        },
                                                    CountryID:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"
                                                        },      
                                                    ContactEmail:
                                                        {
                                                            required: "{$page['Errors']['email']|escape:'html'}",
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },  
                                                    ContactPhone:
                                                        {
                                                            required: "{$page['Errors']['phone']|escape:'html'}"
                                                        },     
                                                    AccountNumber:
                                                        {
                                                            required: "{$page['Errors']['account_number']|escape:'html'}"
                                                        },  
                                                        
                                                    PaymentDiscount:
                                                        {
                                                            number: "{$page['Errors']['payment_incentive']|escape:'html'}"
                                                        },  
                                                    PaymentTerms:
                                                        {
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                    SageNominalCode:
                                                        {
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
						    DefaultTurnaroundTime: {
							digits: "{$page['Errors']['digits']|escape:'html'}",
							maxlength: "Number must be between 1 and 99"
                                                    },
                                                    ForceReferralNumber:
                                                        {
                                                            required: "{$page['Errors']['force_referral_number']|escape:'html'}"
                                                        }
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/OrganisationSetup/clients/update/',
                            updateDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/Clients/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "ClientsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'ClientsResultsPanel',
                            htmlTableId:     'ClientsResults',
                            fetchDataUrl:    '{$_subdomain}/OrganisationSetup/ProcessData/Clients/fetch/'+urlencode("{$nId}"),
                            formCancelButton:'cancel_btn',
			    //pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command'


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="ClientsTopForm" name="ClientsTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="ClientsResultsPanel" >
                    
                    {if $SupderAdmin eq true} 
                        <form id="nIdForm" class="nidCorrections">
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        </form>
                    {/if} 
                    
                    <table id="ClientsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th></th> 
                                        <th width="30%" title="{$page['Text']['name']|escape:'html'}" >{$page['Text']['name']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['network']|escape:'html'}" >{$page['Text']['network']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['account_number']|escape:'html'}"  >{$page['Text']['account_number']|escape:'html'}</th>
                                        <th width="15%" title="{$page['Text']['invoice_to_network']|escape:'html'}"  >{$page['Text']['invoice_to_network']|escape:'html'}</th>
                                        <th width="25%" title="{$page['Text']['assigned_sc']|escape:'html'}"  >{$page['Text']['assigned_sc']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



