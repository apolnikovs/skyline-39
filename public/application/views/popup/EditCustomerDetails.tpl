<script type="text/javascript">   
$(document).ready(function() {
/* =======================================================
*
* Initialise input auto-hint functions...
*
* ======================================================= */

$('.auto-hint').focus(function() {
$this = $(this);
if ($this.val() == $this.attr('title')) {
$this.val('').removeClass('auto-hint');
if ($this.hasClass('auto-pwd')) {
    $this.prop('type','password');
}
}
} ).blur(function() {
$this = $(this);
if ($this.val() == '' && $this.attr('title') != '')  {
$this.val($this.attr('title')).addClass('auto-hint');
if ($this.hasClass('auto-pwd')) {
    $this.prop('type','text');
}
}         
} ).each(function(){
$this = $(this);
if ($this.attr('title') == '') { return; }
if ($this.val() == '') { 
if ($this.attr('type') == 'password') {
    $this.addClass('auto-pwd').prop('type','text');
}
$this.val($this.attr('title')); 
} else { 
$this.removeClass('auto-hint'); 
}
$this.attr('autocomplete','off');
} );



/* =======================================================
*
* set tab on return for input elements with form submit on auto-submit class...
*
* ======================================================= */

$('input[type=text],input[type=password]').keypress( function( e ) {
if (e.which == 13) {
$(this).blur();
if ($(this).hasClass('auto-submit')) {
    $('.auto-hint').each(function() {
        $this = $(this);
        if ($this.val() == $this.attr('title')) {
            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
            if ($this.hasClass('auto-pwd')) {
                $this.prop('type','password');
            }
        }
    } );
    $(this).get(0).form.onsubmit();
} else {
    $next = $(this).attr('tabIndex') + 1;
    $('[tabIndex="'+$next+'"]').focus();
}
return false;
}
} ); 


$(document).on("blur", "#jbContactHomePhone", function() {
    $("#jbContactHomePhone").val($("#jbContactHomePhone").val().replace(/\s+/g, ''));
});


});
</script>



<form action="#" id="EditCustomerDetails" name="EditCustomerDetails" method="post">
    <fieldset>      
        <legend title="AP7111">Edit Customer Details</legend>
        <p class="error" style="display:none;"></p>
        <p>
        <label class="fieldLabel" for="CustomerTitleID" >{$page['Labels']['title']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <select  name="CustomerTitleID" id="CustomerTitleID" class="text auto-hint" >

                <option value="" {if $CustomerTitleID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                {foreach from=$Skyline->getTitles() item=stitle}
                <option value="{$stitle.TitleID}" {if $CustomerTitleID eq $stitle.TitleID}selected="selected"{/if} >{$stitle.Name|escape:'html'}</option>
                {/foreach}

            </select> 
        </p>
        <p>
           <label for="ContactFirstName">{$page['Labels']['forename']|escape:'html'}:</label>&nbsp;&nbsp;
             <input type="text" class="text" name="ContactFirstName" value="{$ContactFirstName|escape:'html'}"  id="ContactFirstName" >
             
        </p>
        
        <p>
            <label for="ContactLastName">{$page['Labels']['surname']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <input type="text" class="text" name="ContactLastName" value="{$ContactLastName|escape:'html'}" id="ContactLastName" >          
        </p> 
        <p>
            <label for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <input  class="text uppercaseText" style="width: 130px;" type="text" name="PostalCode" id="PostalCode" value="{$PostalCode|escape:'html'}" >&nbsp;

            <input type="button" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  value="Click to find Address" />
            <span id="fetch" style="display: none" class="blueText" >({$page['Text']['fetching']|escape:'html'})</span>

        </p>

        <p id="selectOutput" ></p>


        <p id="jb_p_house_name">
            <label for="jbBuildingName" >{$page['Labels']['building_name']|escape:'html'}:</label>
	    &nbsp;&nbsp;
            <input type="text" class="text"  name="BuildingNameNumber" value="{$BuildingNameNumber|escape:'html'}" id="jbBuildingName" >          
        </p>

        <p id="jb_p_street"  >
            <label for="jbStreet" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <input type="text" class="text"  name="Street" value="{$Street|escape:'html'}" id="jbStreet" >          
        </p>

        <p id="jb_p_area"  >
            <label for="jbArea" >{$page['Labels']['area']|escape:'html'}:</label>&nbsp;&nbsp;
             <input type="text" class="text" name="LocalArea" value="{$Area|escape:'html'}" id="jbArea" >          
        </p>


        <p id="jb_p_town"  >
            <label for="jbCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <input type="text" class="text" name="TownCity" value="{$City|escape:'html'}" id="jbCity" >          
        </p>

        <p id="jb_p_county"  >
            <label for="jbCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:</label>&nbsp;&nbsp;
             


            <select name="CountyID" id="jbCounty"  >
                <option value="" id="country_0_county_0" {if $CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                {foreach $Skyline->getCountriesCounties() as $country}

                <optgroup label="{$country.Name}"  >
                    {foreach $country.Counties as $county}
                    <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                    {/foreach}
                </optgroup>

                {/foreach}

            </select>

        </p>
        {if $ShowCountry eq 1}    
            
            
             <p id="jb_p_country" >
                <label for="jbCountry" >{$page['Labels']['country']|escape:'html'}:{if $CountryMandatory eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; 
                    <select name="CountryID" id="jbCountry" >
                    <option value="" {if $CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $Skyline->getCountriesCounties() as $country}

                        <option value="{$country.CountryID}" {if $CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                    {/foreach}

                </select>
            </p>
            
            
        {/if} 
        
	<p>
	    <label for="jbContactEmail">{$page['Labels']['email']|escape:'html'}:<sup id="jbContactEmailSup">*</sup></label>
            &nbsp;&nbsp; 
	    <input type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$ContactEmail|escape:'html'}" id="jbContactEmail" />
            <br>
            <span class="text jobBookingValueInner" >
		&nbsp;&nbsp;<input type="checkbox" name="NoContactEmail" {if $ContactEmail == ""}checked="checked"{/if} id="jbNoContactEmail">{$page['Text']['no_email']|escape:'html'}
            </span>
	    <br>
	</p>
        
        <p>
            <label for="jbContactHomePhone">{$page['Labels']['daytime_phone']|escape:'html'}:<sup>*</sup></label>
            &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactHomePhone" value="{$ContactHomePhone|escape:'html'}" id="jbContactHomePhone" >
            <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactWorkPhoneExt" value="{$ContactWorkPhoneExt|escape:'html'}" id="ContactWorkPhoneExt" />
        </p>
        
        <p>
            <label for="jbContactMobile" >{$page['Labels']['mobile_no']|escape:'html'}:</label>
            &nbsp;&nbsp; <input  type="text" class="text auto-hint" name="ContactMobile" class="text auto-hint" id="jbContactMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="{$ContactMobile|escape:'html'}" >
              
            
        </p>
        
        {if $SendServiceCompletionText eq 'AlternativeMobileNo'}
            <p>
                <label for="ContactAltMobile" >{$page['Labels']['alt_sms_no']|escape:'html'}:</label>
                &nbsp;&nbsp; <input  type="text" class="text auto-hint" name="ContactAltMobile" class="text auto-hint" id="ContactAltMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="{$ContactAltMobile|escape:'html'}" >

            </p>
        {/if}

       <p><br></p>
        
        <p style="width:100%; text-align:center; margin-bottom:10px;">
            <button id="EditCustomerDetailsSave" class="btnConfirm" style="margin-right:10px; width:60px;"><span class="label">Save</span></button>
            <button class="btnCancel cancel last" style="width:60px;"><span class="label">Cancel</span></button>
        </p>
    </fieldset>
</form>