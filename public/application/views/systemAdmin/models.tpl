{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Models}
    {$fullscreen=true}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
     <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
     <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[4]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(3)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(3)', nRow).html( $statuses[0][0] );
        }
        
    }
    
    
     /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
   
 

    $(document).ready(function() {
        $("#mId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/');
            }
        });
        $("#uId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/');
            }
        });
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/');
            }
        });
        $("#cId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/');
            }
        });



                   //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {
                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');

                                });


                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                                           
                                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                      
                      
                      
                     /* Add a change handler to the client dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the client dropdown - ends here*/ 
                      
                      
                      
                       /* Add a change handler to the unit type dropdown - strats here*/
                        /*$(document).on('change', '#uId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the unit type dropdown - ends here*/ 
                      
                      
                      
                       /* Add a change handler to the manufacturer dropdown - strats here*/
                        /*$(document).on('change', '#mId', 
                            function() {
                                                           
                                $(location).attr('href', '{$_subdomain}/ProductSetup/models/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+urlencode($("#uId").val())+"/"+urlencode($("#mId").val())+'/');  
                            }      
                        );*/
                      /* Add a change handler to the manufacturer dropdown - ends here*/ 
                      
                  
              

                    
var oTable = $('#ModelsResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		
   "sAjaxSource": '{$_subdomain}/ProductSetup/loadModelsTable/{if $showSpUnmodified|default:''===true}showSpUnmodified=show/{/if}'+$("#nId").val()+'/'+$("#cId").val()+'/'+$("#uId").val()+'/'+$("#mId").val()+'/',
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#ModelsResults').show();
                               
			} );
                        },
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": "Search within results"
                            },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
"iDisplayLength" : 10,

"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               true
                            
		] 
               
   
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#ModelsResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
      /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/processModel/id="+anSelected[0].id,
                        title: "Edit Model",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
    
    
    
    
	$('input[id^=inactivetick]').click( function() {
         $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/");
                 }
	} );  
        var v=0;
	$('a[id^=showpendingtick]').click( function() {
        v++;
        if(v==1){
                 $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/pending=1/");
                 }else{
                 v=0;
                  $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/");
                 }
	} );                            
    
	$('input[id^=unaprovedtick]').click( function() {
        $('#inactivetick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/unaproved=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/");
                 }
	} );                            
          
          
          
          


                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#ModelsResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/processModel/id="+anSelected[0].id,
                        title: "Edit Model",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  





                   
               // oTable.fnReloadAjax('{$_subdomain}/ProductSetup/loadModelsTable/'+urlencode("{$uId}")+"/"+urlencode("{$mId}"));
             
             /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/ProductSetup/deleteModel/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             
              //$('#ModelsResults_filter').css({ 'float':'right','top':'28px','position':'relative'});     
              $('#container-main').css({ 'min-width':'1076px'});     
          
          
          
          
//help icons init
$('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });
          
    $('input[id^=showSpUnmodified]').click( function() {
      
     if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/showSpUnmodified=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#ModelsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadModelsTable/");
                 }
                
                    
		
                 
	} );        
          
          
          

    });//end doc ready
    
    
    //displaying table pref colorbox
function showTablePreferences(){
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/tableDisplayPreferenceSetup/page=models/table=model",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
function modelInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/processModel",
                        title: "Insert Model",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
 function modelEdit()
{
    tt=oTable['ModelsResults'];
    var anSelected = fnGetSelected( tt );
                var aData = tt.fnGetData(anSelected[0]); 
                alert(aData[0]);
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/processModel/"+aData[0],
                        title: "Edit Model",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}

    </script>

    {/block}

    {block name=body}
     <div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>

    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" style="width:100%;min-width: 1076px;">

               <div class="ServiceAdminTopPanel" >
                    <form id="ModelsTopForm" name="ModelsTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
                   {if isset($pendingNo)&&$pendingNo>0}
                        <div style="text-align: center;background:#FFE8F1;border:1px solid red;padding: 4px;margin-top:10px;margin-bottom: 10px;width:300px;">
                            <a id="showpendingtick" href="#">Show Pending</a>
                            <span style="color:red">{$pendingNo}</span> {$page['Labels']['tableName']} Need Approval
                        </div>
                        {/if}
                <div class="ServiceAdminResultsPanel" id="ModelsResultsPanel" >
                    <div id="selectBoxesDiv" style="float:left;position:relative;top:-8px;">
                        <div style="float:left;position:relative;">
                   <form name="listsForm" >  
                    {$page['Labels']['manufacturer_label']|escape:'html'}
                    <select name="mId" id="mId" class="topDropDownList" >
                        <option value="" {if $mId eq ''}selected="selected"{/if}>{$page['Text']['select_manufacturer']|escape:'html'}</option>

                        {foreach $manufacturers as $manufacturer}

                            <option value="{$manufacturer.ManufacturerID}" {if $mId eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                        {/foreach}
                        
                    </select><br><span style="padding-right: 22px;">{$page['Labels']['unit_type_label']|escape:'html'}</span>
                         <select name="uId" id="uId" class="topDropDownList" >
                            <option value="" {if $uId eq ''}selected="selected"{/if}>{$page['Text']['select_unit_type']|escape:'html'}</option>

                            {foreach $unitTypes as $unitType}

                                <option value="{$unitType.UnitTypeID}" {if $uId eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                            {/foreach}
                        </select>
                    {if $SupderAdmin eq true} 
                       
                      
                    </div>
                        
                        <div style="float:left;position:relative;display:none;top:-8px">                        
                        &nbsp; <select name="cId" id="cId" class="topDropDownList" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                       <br>                        
                   
                       &nbsp; <select name="nId" id="nId" class="topDropDownList" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                    {/if}
                    
                     
                    </div>
                    </div>
                    
                    <div style="float:left;position:relative;top:15px;margin-left:10px">
                        <button style="width:100px" onclick="window.location='{$_subdomain}/LookupTables/accessory'" class="gplus-blue">Accesory</button>
                        <button style="width:100px" class="gplus-blue" onclick="window.location='{$_subdomain}/LookupTables/productCode'">Product Code</button>
                        <button style="width:100px" onclick="window.location='{$_subdomain}/LookupTables/colour'" class="gplus-blue">Colour</button>
                    </div>
                    <div style="float:left;position: relative"><input {if $showSpUnmodified|default:''===true} checked="checked"  {/if}type="checkbox" name="showSpUnmodified" id="showSpUnmodified" > Display Unmodified Records <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerShowUnmodifiedHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" ></div>
                    <div style="width:100%;height:10px;">&nbsp</div>
                   
                        <table style="display:none;margin-top:10px;" id="ModelsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                          <thead>
			    <tr>
                                {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px"></th>
			    </tr>
                        </thead>
                            <tbody>
 <div style="text-align:left" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                            </tbody>
                        </table>  
                            
                             <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                     {if $activeUser=="SuperAdmin"} <input id="unaprovedtick"  type="checkbox" > Show Unapproved {/if}
                    </div>
                     </form>
                            
                </div>        

                
                
             
                   
                            <input style="display:none" type="button" name="updateButtonId" id="updateButtonId" > 
                   
                 
                 
                 
 <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" onclick="modelEdit()" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="modelInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
    </div>
                             
         <div>
                <hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/productSetup'">Finish</button>
                </div>               



{/block}



