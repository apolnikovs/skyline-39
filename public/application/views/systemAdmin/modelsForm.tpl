
    
    <script type="text/javascript" >
        
        $(function() {
    $( "#tabs" ).tabs();
  });
              
        
    $(document).ready(function() {
        $("#ManufacturerID").combobox();
        $("#UnitTypeID").combobox();
        $("#Accessory").combobox();
        $("#ProductCode").combobox();
        $("#Colour").combobox();
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });
        
        
    </script>
    
    
    <div id="ModelsFormPanel" class="SystemAdminFormPanel" >
    
                <form id="ModelsForm" name="ModelsForm" method="post"  action="{$_subdomain}/ProductSetup/saveModel" class="inline" >
                    
                    
       <input type="hidden" name="mode" value="{$mode}">
       
       {if isset($data)}
       <input type="hidden" name="id" value="{if !isset($data['ServiceProviderModelID'])}{$data['ModelID']}{else}{$data['ServiceProviderModelID']}{/if}">
      
       {/if}
        <input type="hidden" name="ServiceProviderID" value="{$ServiceProviderID}">
        <input type="hidden" name="CreatedDate" value="{$data.CreatedDate|default:''}">
       
                    
                    
       
                <fieldset>
                    <legend title="" >{if $mode=="insert"}Insert Model{else}{$form_legend|escape:'html'}{/if}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                               <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Model Details</a></li>
    {if isset($data)&&$mode!="copyNew"}
    <li><a onclick="loadAccessoryList('Accessory')" href="#tabs-2">Accesory</a></li>
    <li><a onclick="loadAccessoryList('ProductCode')" href="#tabs-3">Product Code</a></li>
    <li><a onclick="loadAccessoryList('Colour')" href="#tabs-4">Colour</a></li>
    {/if}
   </ul>
                                  
                                   <div id="tabs-1" class="SystemAdminFormPanel inline">
                         <p>
                               <label ></label>
                               <span class="topText cardTopLabel saFormSpan" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                         </p>
                       
                         
                         <p>
                            
                             <label class="cardLabel" for="ModelNumber" >{$page['Labels']['model_number']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ModelNumber" value="{if isset($data)}{$data.ModelNumber|escape:'html'}{/if}" id="ModelNumber" >
                        
                         </p>
                         
                         
                         <p>
                            
                             <label class="cardLabel" for="ModelDescription" >{$page['Labels']['description']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ModelDescription" value="{if isset($data)}{$data.ModelDescription|escape:'html'}{/if}" id="ModelDescription" >
                        
                         </p>
                         
                         
                         {if $activeUser=="SuperAdmin"}
                         <p>
                            <label class="cardLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if isset($data)}{if $data.ManufacturerID eq ''}selected="selected"{/if}{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ManufacturerID}" {if isset($data)&& $data.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                         
                         {else}
                             
                            <p>
                            <label class="cardLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if isset($data)}{if $data.ServiceProviderManufacturerID eq ''}selected="selected"{/if}{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ServiceProviderManufacturerID}" {if isset($data)&& $data.ServiceProviderManufacturerID eq $manufacturer.ServiceProviderManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>  
                             
                          {/if}   
                          <p>
                            <label class="cardLabel" for="UnitTypeID" >{$page['Labels']['unit_type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="UnitTypeID" id="UnitTypeID" class="text" >
                                <option value="" {if isset($data)}{if $data.UnitTypeID eq ''}selected="selected"{/if}{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $unitTypes as $unitType}

                                    <option value="{$unitType.UnitTypeID}" {if isset($data)&& $data.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                          
                         
                         
                          
                             
                        <p>
                            <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:</label>

                            &nbsp;&nbsp; 

                               

                                    <input  type="radio" name="Status"  value="Active" {if (isset($data)&& $data.Status eq "Active")||!isset($data)} checked="checked" {/if}  /> <label class="saFormSpan">Active</label>
                                    <input  type="radio" name="Status"  value="In-active" {if isset($data)&& $data.Status eq "In-active"} checked="checked" {/if}  /> <label class="saFormSpan">In-active</label>

                                
                        </p>
                              {if isset($data)&&$activeUser=="SuperAdmin"&&$data.ApproveStatus!="Approved"}
                         <p>
                                <label class="cardLabel" for="ApproveStatus" >Account Type:<sup>*</sup></label>

                                &nbsp;&nbsp; 

                                   

					<span class="formRadioCheckText"  class="saFormSpan">
					    <input  type="radio" name="ApproveStatus"  value="Approved"   /><label >Global</label>&nbsp;
					    <input  type="radio" name="ApproveStatus"  value="NotApproved"   /><label >{$ancronym} Specific</label>&nbsp;
					</span>

                                     
                            </p>
                            {/if}
                        
                          <p>
                            
                             <label class="cardLabel" for="Features" >Features:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="Features" value="{if isset($data)}{$data.Features|escape:'html'}{/if}" id="Features" >
                        
                         </p>
                         <p>
                            
                             <label class="cardLabel" for="IMEILengthFrom" >IMEI Length - From:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="IMEILengthFrom" value="{if isset($data)}{$data.IMEILengthFrom|escape:'html'}{/if}" id="IMEILengthFrom" >
                        
                         </p>
                         <p>
                            
                             <label class="cardLabel" for="IMEILengthTo" >IMEI Length - To:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="IMEILengthTo" value="{if isset($data)}{$data.IMEILengthTo|escape:'html'}{/if}" id="IMEILengthTo" >
                        
                         </p>
                         <p>
                            
                             <label class="cardLabel" for="MSNLengthFrom" >MSN Length - From:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="MSNLengthFrom" value="{if isset($data)}{$data.MSNLengthFrom|escape:'html'}{/if}" id="MSNLengthFrom" >
                        
                         </p>
                        
                         <p>
                            
                             <label class="cardLabel" for="MSNLengthTo" >MSN Length - To:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="MSNLengthTo" value="{if isset($data)}{$data.MSNLengthTo|escape:'html'}{/if}" id="MSNLengthTo" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="WarrantyRepairLimit" >Warranty Repair Limit:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="WarrantyRepairLimit" value="{if isset($data)}{$data.WarrantyRepairLimit|escape:'html'}{/if}" id="WarrantyRepairLimit" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="ExcludeFromRRCRepair" >Exclude From RRC Repair:</label>
                                &nbsp;&nbsp; 
                             <input  type="checkbox"  name="ExcludeFromRRCRepair" {if isset($data)}{if $data.ExcludeFromRRCRepair=="Yes"}checked=checked{/if}{/if} value="Yes" id="ExcludeFromRRCRepair" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="AllowIMEIAlphaChar" >Allow IMEI Alpha Characters:</label>
                                &nbsp;&nbsp; 
                             <input  type="checkbox"  name="AllowIMEIAlphaChar" {if isset($data)}{if $data.AllowIMEIAlphaChar=="Yes"}checked=checked{/if}{/if} value="Yes" id="AllowIMEIAlphaChar" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="UseReplenishmentProcess" >Use Replenishment Process:</label>
                                &nbsp;&nbsp; 
                             <input  type="checkbox"  name="UseReplenishmentProcess" {if isset($data)}{if $data.UseReplenishmentProcess=="Yes"}checked=checked{/if}{/if} value="Yes" id="UseReplenishmentProcess" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="HandsetWarranty1Year" >Handset Warranty 1 Year Only:</label>
                                &nbsp;&nbsp; 
                             <input  type="checkbox"  name="HandsetWarranty1Year" {if isset($data)}{if $data.HandsetWarranty1Year=="Yes"}checked=checked{/if}{/if} value="Yes" id="HandsetWarranty1Year" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="ReplacmentValue" >Replacement Value:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ReplacmentValue" value="{if isset($data)}{$data.ReplacmentValue|escape:'html'}{/if}" id="ReplacmentValue" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="ExchangeSellingPrice" >Exchange Selling Price:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ExchangeSellingPrice" value="{if isset($data)}{$data.ExchangeSellingPrice|escape:'html'}{/if}" id="ExchangeSellingPrice" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="LoanSellingPrice" >Loan Selling Price:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="LoanSellingPrice" value="{if isset($data)}{$data.LoanSellingPrice|escape:'html'}{/if}" id="LoanSellingPrice" >
                        
                         </p>
                          <p>
                            
                             <label class="cardLabel" for="RRCOrderCap" >RRC Order Cap:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="RRCOrderCap" value="{if isset($data)}{$data.RRCOrderCap|escape:'html'}{/if}" id="RRCOrderCap" >
                        
                         </p>
                         

                        <p>

                            <span class= "bottomButtons" >

                                
                                <input type="hidden" name="EndDate"  value="{if isset($data)}{$data.EndDate|escape:'html'}{/if}" >


                               


                            </span>

                        </p>

                         
                                   </div>
                                 {if isset($data)&&$mode!="copyNew"}
                            <div id="tabs-2" class="SystemAdminFormPanel inline">
                             <p>
                                 <label class="cardLabel"  for="Accessory" style="width:100px" >Accessory:</label>
                            &nbsp;&nbsp; 
                                <select name="Accessory" id="Accessory" class="text" style="margin-bottom:25px" >
                                <option value="" >{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $accessories as $s}

                                    <option value="{$s.AccessoryID}" >{$s.AccessoryName|escape:'html'}</option>

                                {/foreach}
                                
                                </select>&nbsp;<img style="cursor:pointer" src="{$_subdomain}/images/add_icon.png" onclick="additem('Accessory')">
                                
                        </p>
                        <p id="AccessoryP"></p>
                             <div style="text-align:center" id="ajaxLoader" class="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                            </div>
                            <div id="tabs-3" class="SystemAdminFormPanel inline">
                            <p>
                                 <label class="cardLabel"  for="ProductCode" style="width:100px" >Product Code:</label>
                            &nbsp;&nbsp; 
                                <select name="ProductCode" id="ProductCode" class="text" style="margin-bottom:25px" >
                                <option value="" >{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $productCodes as $s}

                                    <option value="{$s.ProductCodeID}" >{$s.ProductCodeName|escape:'html'}</option>

                                {/foreach}
                                
                                </select>&nbsp;<img style="cursor:pointer" src="{$_subdomain}/images/add_icon.png" onclick="additem('ProductCode')">
                                
                        </p>
                            <p id="ProductCodeP"></p>
                             <div style="text-align:center" id="ajaxLoader" class="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                            </div>
                            <div id="tabs-4" class="SystemAdminFormPanel inline">
                            
                         <p>
                                 <label class="cardLabel"  for="Colour" style="width:100px" >Colours:</label>
                            &nbsp;&nbsp; 
                                <select name="Colour" id="Colour" class="text" style="margin-bottom:25px" >
                                <option value="" >{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $colours as $s}

                                    <option value="{$s.ColourID}" >{$s.ColourName|escape:'html'}</option>

                                {/foreach}
                                
                                </select>&nbsp;<img style="cursor:pointer" src="{$_subdomain}/images/add_icon.png" onclick="additem('Colour')">
                                
                        </p>
                            <p id="ColourP"></p>
                             <div style="text-align:center" id="ajaxLoader" class="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                            
                            </div>
                             
                             {/if}
                             </div> 
                         <p>
                <hr>
                               
                                <div style="height:20px;margin-bottom: 10px">
                                <button type="submit" style="float: left" class="gplus-blue">Save</button>
                                <button type="button" onclick="$.colorbox.close();" style="float:right" class="gplus-blue">Cancel</button>
                                </div>
            </p>    
                </fieldset>    
                       
                </form>      
                                
                        </div>
                           
       {if isset($data.ServiceProviderModelID)}
     <script>

                        function additem(item){
                        item=item;
                       $('#'+item+'P').html('');
                       $('.ajaxLoader').show();
                         $.post("{$_subdomain}/ProductSetup/add"+item+"/",{ ServiceProviderModelID:{if isset($data)}{$data.ServiceProviderModelID}{/if} ,ACCID:$('#'+item).val()},
function(data) {

removeAccessoryOption($('#'+item).val(),item);
loadAccessoryList(item);
});
                        }
function removeAccessoryOption(val,item){
$("#"+item+" option[value="+val+"]").remove();
}

function loadAccessoryList(item){
$('#ajaxLoader').show();
$('#'+item+'P').html('');
 $.post("{$_subdomain}/ProductSetup/load"+item+"/",{ ServiceProviderModelID:{if isset($data)}{$data.ServiceProviderModelID}{/if}},
function(data) {

en=jQuery.parseJSON(data);
$.each(en, function(key, value) { 
removeAccessoryOption(en[key]['id'],item)
$('#'+item+'P').append("<label class='cardLabel'></label><label title='Click to Remove' class='saFormSpan' style='cursor:pointer' onclick=removeAccessory("+en[key]['id']+",'"+item+"')>"+en[key]['name']+"</label><br>");
});
$('.ajaxLoader').hide();
});
}
function removeAccessory(id,item){

if (confirm('Are you sure you want to remove this?')) {
    $('.ajaxLoader').show();
   $.post("{$_subdomain}/ProductSetup/del"+item+"/",{ ServiceProviderModelID:{if isset($data)}{$data.ServiceProviderModelID}{/if} ,ACCID:id},
function() {
loadAccessoryList(item);
});
} else {
    // Do nothing!
}
}
                    
                        </script>
                 
{/if}
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >
                        

                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
