<?php

require_once('CustomSmartyController.class.php');

/**
 * Description
 *
 * Appointment Diary
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.01
 * 
 * Changes
 * Date        Version Author                 Reason
 * 14/09/2012  1.00    Nageswara Rao Kanteti  Initial Version      

 ******************************************************************************/

class AppointmentDiaryController extends CustomSmartyController {
    
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
       // $this->smarty->assign('_theme', 'skyline');
        
       if (isset($this->session->UserID)) {
           
             
           

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            
            
            if($this->user->ClientID || $this->user->BranchID)
            {
                $this->redirect('index',null, null);
            }
            
            //$this->log(var_export($this->user, true));

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            
             $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            
            $topLogoBrandID = $this->user->DefaultBrandID;

        } else {
            
            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', '');   
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index',null, null);
            
        } 
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
        
        
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
       
    }
       
    
    
    
    
    /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
   
     **************************************************************************/
     public function ProcessDataAction($args)   
     {
         //$this->log($_POST,"deftimes");
            $modelName = isset($args[0])?$args[0]:'';
            $type      = isset($args[1])?$args[1]:'';
            $data=$_POST;
         if(isset($data['df_count'])&&$data['df_count']>0){
             
            $diary=$this->loadModel('Diary');
            $diary->deleteDeferedTime($data['ServiceProviderID']);
            for($i=0;$i<$data['df_count'];$i++){
                $diary->saveDeferredTime($data['ServiceProviderID'],$data['EarliestWorkday_'.$i],$data['LatestWorkday_'.$i],$data['DeffPostcodeWork_'.$i],$data['EarliestWeekend_'.$i],$data['LatestWeekend_'.$i],$data['DeffPostcodeWeekEnd_'.$i]);
            }
         }
            if($modelName && $modelName!='')
            {
                
                $this->page =  $this->messages->getPage(lcfirst($modelName), $this->lang);
                
                
                $model  = $this->loadModel($modelName);
                
                if($type=='fetch')
                {
                    $_POST['firstArg']  = isset($args[2])?$args[2]:'';
                    $_POST['secondArg'] = isset($args[3])?$args[3]:'';
                    
                    $result     =   $model->fetch($_POST);
                }
                else
                {
                   // $this->log(var_export($_POST, true));
                    
                    
                    $result     = $model->processData($_POST);
                }    
                echo json_encode( $result );
           }
            return;
     }
    
    
    
    
    
     
    /**
     * Description
     * 
     * This method is used for to add/edit engineers of service provider
     * 
     * @param array $args
     * @return void  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
   
        public function indexAction( $args ) {
            
           //  $EmailModel = $this->loadModel('Email');  
             
           // $EmailModel->sendOneTouchJobCancellationEmail(48, 'Skyline', 'c.berry@pccsuk.com');
           // $EmailModel->sendAuthorisationEmail(48, 'unable_to_authorise_repair', 'c.berry@pccsuk.com');
            //$EmailModel->sendAuthorisationEmail(48, 'query_raised_against_authorisation', 'c.berry@pccsuk.com');
           // $EmailModel->sendAppointmentDetailsEmail(1, "reminder");
           // $EmailModel->sendAppointmentDetailsEmail(1, "date_confirmed");
          //  $EmailModel->sendJobCompleteEmail(48);
           // $EmailModel->sendAppraisalRequestEmail(48);
           //  $EmailModel->sendAppointmentEmail(1, 'Booked', 'c.berry@pccsuk.com');
            // $EmailModel->sendAppointmentEmail(1, 'Cancelled', 'c.berry@pccsuk.com');
             
             
            
             $method = isset($args[0])?$args[0]:false;
             $rowID  = isset($args[1])?$args[1]:false;
             $showUpdatePage = false;
             
             $tInfo = isset($args["tInfo"])?$args["tInfo"]:false;
            
        
             $this->page    = $this->messages->getPage('appointmentDiary', $this->lang);
             $this->smarty->assign('page', $this->page);
         

            $Skyline = $this->loadModel('Skyline');
   
            $serviceProvidersList = $Skyline->getServiceProviders();
             
           
            $lunchRestPeriod = array();
            for($i=0;$i<=120;$i=$i+5)
            {
                if($i<10 && $i)
                {
                    $Value = "0".$i;
                }    
                else
                {
                   $Value = $i; 
                }    
                
                $lunchRestPeriod[] = array("Text"=>$Value, 'Value'=>$Value);
            }
            
            if($method=="update" && $rowID)
            {
                
                $Engineer = $this->loadModel('Engineers');
                 
                $datarow = $Engineer->fetchRow(array("ServiceProviderEngineerID"=>$rowID));
                if(is_array($datarow))
                {    
                       $args['spID'] = $datarow['ServiceProviderID'];
                       
                       
                       $datarow['LunchPeriodFrom'] = substr($datarow['LunchPeriodFrom'], 0, -3);
                       
                       if($datarow['LunchPeriodFrom']=='00:00')
                       {
                           $datarow['LunchPeriodFrom'] = '';
                       }    
                       
                       $datarow['LunchPeriodTo'] = substr($datarow['LunchPeriodTo'], 0, -3);
                       
                       if($datarow['LunchPeriodTo']=='00:00')
                       {
                           $datarow['LunchPeriodTo'] = '';
                       }    
                       
                       
                       $showUpdatePage = true;
                       
                       $weekArray = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                       $WeekNo    = 1;
                       $DayNo     = 1;
                       $datarow['FocusField'] = "EngineerFirstName";
                      
                       if(isset($args['d']) && strlen($args['d'])==10 && $args['d']>=date("Y-m-d", strtotime('monday this week')))
                       {

                           $datetime1 = new DateTime(date("Y-m-d", strtotime('monday this week')));
                           $datetime2 = new DateTime($args['d']);
                           $interval = $datetime1->diff($datetime2);


                           $WeekNo = floor(($interval->days/7))+1; 
                           $DayNo  = $interval->days%7;
                           
                           if($WeekNo<1 || $WeekNo>4)
                           {
                               $WeekNo = 1;
                           }
                           $datarow['FocusField'] = $weekArray[$DayNo]."Active".$WeekNo;
                       }
                       
                       
                       $datarow[$WeekNo]['WeekClassName'] = 'weekButtonsActive';
                }
                
                //$this->log($datarow);
                
            }
            
           $SuperAdmin = true; 
           $ServiceProviderID = ''; 
           if($this->user->ServiceProviderID) 
           {
             
               $ServiceProviderID = $this->user->ServiceProviderID;
               $SuperAdmin = false;
           }
           else if(isset($args['spID']))
           {
               $ServiceProviderID = $args['spID'];
           }
           else if(isset($_SESSION['session_spID']) && $_SESSION['session_spID'])
           {
               $ServiceProviderID = $_SESSION['session_spID'];
           }
           else if(count($serviceProvidersList))
           {
               $ServiceProviderID = $serviceProvidersList[0]['ServiceProviderID'];
           
           }
           $_SESSION['session_spID'] =  $ServiceProviderID;
           
           $ServiceProviderSkillset = $this->loadModel('ServiceProviderSkillsSet');
           $skillSet                = $ServiceProviderSkillset->getServiceProviderSkillset($ServiceProviderID);
           
          
              $this->user->ServiceProviderID=$ServiceProviderID;
            
            if(!$showUpdatePage)
            {
                
                $start_date_time = strtotime('monday this week'); 
                
                 $datarow  = array(

                    "ServiceProviderEngineerID"=>'',
                    "EngineerFirstName"=>"",
                    "EngineerLastName"=>"",
                    "ServiceBaseUserCode"=>"",
                    "EmailAddress"=>"",
                    "MobileNumber"=>"", 
                    "RouteColour"=>"FFFFFF",
                    "SpeedFactor"=>100, 
                    "LunchBreakDuration"=>"",
                    "LunchPeriodFrom"=>"",
                    "LunchPeriodTo"=>"",
                    "Status"=>'Active',
                    "start_date"=>date("Y-m-d", $start_date_time),
                    "StartHomePostcode"=>'',
                    "SentToViamente"=>'',
                    "FocusField"=> "EngineerFirstName",
                    "EngineerImportance"=>"",
                    "ViamenteExclude"=>"No",
                    "ExcludeFromAppTable"=>"No",
                    "ViamenteStartType"=>"Flexible",
                    "SetupType"=>"Unique",
                    "ReplicateType"=>"FourWeeksOnly",
                    "ReplicateStatusFlag"=>'No',
                    "ReplicatePostcodeAllocation"=>'Yes' ,
                    "AppsBeforeOptimise"=>0 ,
                    "PrimarySkill"=>null, 
                    "BrownEngLeft"=>0, 
                    "WhiteEngLeft"=>0 
                     

                );
                
                 
                for($i=1;$i<=4;$i++)
                {
                    $datarow[$i]  = array(

                        
                        "WeekClassName"=>"weekButtons",
                        "WeekName"=>$this->page['Labels']['week']." ".date("W", strtotime("+".(($i-1)*7)." day", $start_date_time)),
                        "MondayLabel"=>date("D d M", strtotime("+".(($i-1)*7)." day", $start_date_time)),
                        "MondayDate"=>date("Y-m-d", strtotime("+".(($i-1)*7)." day", $start_date_time)),
                        "MondayActive"=>"",
                        "MondayStartShift"=>"",
                        "MondayEndShift"=>"",
                        "MondayStartPostcode"=>"",
                        "MondayEndPostcode"=>"",
                        "TuesdayLabel"=>date("D d M", strtotime("+".((($i-1)*7)+1)." day", $start_date_time)),
                        "TuesdayDate"=>date("Y-m-d", strtotime("+".((($i-1)*7)+1)." day", $start_date_time)),
                        "TuesdayActive"=>"",
                        "TuesdayStartShift"=>"",
                        "TuesdayEndShift"=>"",
                        "TuesdayStartPostcode"=>"",
                        "TuesdayEndPostcode"=>"",
                        "WednesdayLabel"=>date("D d M", strtotime("+".((($i-1)*7)+2)." day", $start_date_time)),
                        "WednesdayDate"=>date("Y-m-d", strtotime("+".((($i-1)*7)+2)." day", $start_date_time)),
                        "WednesdayActive"=>"",
                        "WednesdayStartShift"=>"",
                        "WednesdayEndShift"=>"",
                        "WednesdayStartPostcode"=>"",
                        "WednesdayEndPostcode"=>"",
                        "ThursdayLabel"=>date("D d M", strtotime("+".((($i-1)*7)+3)." day", $start_date_time)),
                        "ThursdayDate"=>date("Y-m-d", strtotime("+".((($i-1)*7)+3)." day", $start_date_time)),
                        "ThursdayActive"=>"",
                        "ThursdayStartShift"=>"",
                        "ThursdayEndShift"=>"",
                        "ThursdayStartPostcode"=>"",
                        "ThursdayEndPostcode"=>"",
                        "FridayLabel"=>date("D d M", strtotime("+".((($i-1)*7)+4)." day", $start_date_time)),
                        "FridayDate"=>date("Y-m-d", strtotime("+".((($i-1)*7)+4)." day", $start_date_time)),
                        "FridayActive"=>"",
                        "FridayStartShift"=>"",
                        "FridayEndShift"=>"",
                        "FridayStartPostcode"=>"",
                        "FridayEndPostcode"=>"",
                        "SaturdayLabel"=>date("D d M", strtotime("+".((($i-1)*7)+5)." day", $start_date_time)),
                        "SaturdayDate"=>date("Y-m-d", strtotime("+".((($i-1)*7)+5)." day", $start_date_time)),
                        "SaturdayActive"=>"",
                        "SaturdayStartShift"=>"",
                        "SaturdayEndShift"=>"",
                        "SaturdayStartPostcode"=>"",
                        "SaturdayEndPostcode"=>"",
                        "SundayLabel"=>date("D d M", strtotime("+".((($i-1)*7)+6)." day", $start_date_time)),
                        "SundayDate"=>date("Y-m-d", strtotime("+".((($i-1)*7)+6)." day", $start_date_time)),
                        "SundayActive"=>"",
                        "SundayStartShift"=>"",
                        "SundayEndShift"=>"",
                        "SundayStartPostcode"=>"",
                        "SundayEndPostcode"=>"",
                        "MondaySkillsSet"=>array(),
                        "TuesdaySkillsSet"=>array(),
                        "WednesdaySkillsSet"=>array(),
                        "ThursdaySkillsSet"=>array(),
                        "FridaySkillsSet"=>array(),
                        "SaturdaySkillsSet"=>array(),
                        "SundaySkillsSet"=>array()
                  );
                    
                } 
                
                $datarow[1]['WeekClassName'] = 'weekButtonsActive';
                
                
               
            } 
            
            
           if(isset($args['ShowAll']))
           {
              $_SESSION['SessionShowAll'] = $args['ShowAll'];  
           }
           
           if(!isset($_SESSION['SessionShowAll']))
           {
               $_SESSION['SessionShowAll'] = 0;
           }    
          
           
           if(isset($_SESSION['DiaryWizardSetup']) && $_SESSION['DiaryWizardSetup'])
           {
               $this->smarty->assign('DiaryWizardSetup', true);
           }
           else
           {
               $this->smarty->assign('DiaryWizardSetup', false);
           }
           
            
           $this->smarty->assign('lunchRestPeriod', $lunchRestPeriod);
           $this->smarty->assign('skillSet', $skillSet);
           $this->smarty->assign('datarow', $datarow);
           $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
           $this->smarty->assign('ServiceProviderID', $ServiceProviderID);
           $this->smarty->assign('showUpdatePage', $showUpdatePage);
           
           $this->smarty->assign('tInfo', $tInfo);
           $this->smarty->assign('SuperAdmin', $SuperAdmin);
           
           $this->smarty->assign('ShowAll', $_SESSION['SessionShowAll']);
           
           
         //  $temp = $this->loadModel('PostcodeLatLong');
         //  $this->log($temp->getLatLong("AB101Ag"));
           
              $diary = $this->loadModel('Diary');
                             $dat=$diary->getSPAllocationType($ServiceProviderID);
                             $allocOnly=$diary->getDiaryType($ServiceProviderID);
        $this->smarty->assign('dat', $dat);
        $this->smarty->assign('allocOnly',$allocOnly);
           $this->smarty->display('appointmentDiary.tpl'); 


           
           
           

            return;
        
       
      }
      
      
      
      
        /**
       * Description
       * 
       * This method is used for to add/edit skillsSet of service provider
       * 
       * @param array $args
       * @return void  
       * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
       */
   
        public function skillsSetSPAction( $args ) { 
            
             $method = isset($args[0])?$args[0]:false;
             $rowID  = isset($args[1])?$args[1]:false;
             $showUpdatePage = false;
             
             $tInfo = isset($args["tInfo"])?$args["tInfo"]:false;
            
        
             $this->page    = $this->messages->getPage('skillsSetSP', $this->lang);
             $this->smarty->assign('page', $this->page);
         

            $Skyline = $this->loadModel('Skyline');
   
            $serviceProvidersList = $Skyline->getServiceProviders();
             
           
           
            
//            if($method=="update" && $rowID)
//            {
//                $Engineer = $this->loadModel('Engineers');
//                 
//                $datarow = $Engineer->fetchRow(array("ServiceProviderEngineerID"=>$rowID));
//                if(is_array($datarow))
//                {    
//                       $args['spID'] = $datarow['ServiceProviderID'];
//                       
//                       $showUpdatePage = true;
//                }
//                
//            }
            
           $SuperAdmin = true; 
           $ServiceProviderID = ''; 
           if($this->user->ServiceProviderID) 
           {
               $ServiceProviderID = $this->user->ServiceProviderID;
               $SuperAdmin = false;
           }
           else if(isset($args['spID']))
           {
               $ServiceProviderID = $args['spID'];
           }
           else if(isset($_SESSION['session_spID']) && $_SESSION['session_spID'])
           {
                $ServiceProviderID = $_SESSION['session_spID'];
           }
           else if(count($serviceProvidersList))
           {
               $ServiceProviderID = $serviceProvidersList[0]['ServiceProviderID'];
           
           }
           $_SESSION['session_spID'] = $ServiceProviderID; 
           
           $ServiceProviderSkillset = $this->loadModel('ServiceProviderSkillsSet');
           $skillSet                = $ServiceProviderSkillset->getSkillSetRepairSkills();
           
          
            
            
            if(!$showUpdatePage)
            {
                $datarow  = array(

                    "RepairSkillID"=>''

                );
            } 
            
           if(isset($_SESSION['DiaryWizardSetup']) && $_SESSION['DiaryWizardSetup'])
           {
               $this->smarty->assign('DiaryWizardSetup', true);
           }
           else
           {
               $this->smarty->assign('DiaryWizardSetup', false);
           }
            
           
           $this->smarty->assign('skillSet', $skillSet);
           $this->smarty->assign('datarow', $datarow);
           $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
           $this->smarty->assign('ServiceProviderID', $ServiceProviderID);
           $this->smarty->assign('showUpdatePage', $showUpdatePage);
           
           $this->smarty->assign('tInfo', $tInfo);
           $this->smarty->assign('SuperAdmin', $SuperAdmin);
           
           
           
             $diary = $this->loadModel('Diary');
                           
                             $allocOnly=$diary->getDiaryType($ServiceProviderID);
      
        $this->smarty->assign('allocOnly',$allocOnly);
           
           $this->smarty->display('skillsSetSP.tpl'); 



            return;
        
       
      }
      
      
      /**
       * Description
       * 
       * This method is used for to set general defaults for service provider
       * 
       * @param array $args
       * @return void  
       * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
       */
   
        public function diaryDefaultsAction( $args ) { 
            
          // $method = isset($args[0])?$args[0]:false;
          
             
           $tInfo = isset($args["tInfo"])?$args["tInfo"]:false;
           
           $sPage = isset($args["sPage"])?$args["sPage"]:false;
           $cId = isset($args["cId"])?$args["cId"]:1;
            
        
           $this->page    = $this->messages->getPage('diaryDefaults', $this->lang);
           $this->smarty->assign('page', $this->page);
         

           $Skyline = $this->loadModel('Skyline');
   
           $serviceProvidersList = $Skyline->getServiceProviders();
           
           
           
           
             
           $SuperAdmin = true; 
           $ServiceProviderID = ''; 
           if($this->user->ServiceProviderID) 
           {
               $ServiceProviderID = $this->user->ServiceProviderID;
               $SuperAdmin = false;
           }
           else if(isset($args['spID']))
           {
               $ServiceProviderID = $args['spID'];
           }
           else if(isset($_SESSION['session_spID']) && $_SESSION['session_spID'])
           {
               $ServiceProviderID = $_SESSION['session_spID'];
           }
           else if(count($serviceProvidersList))
           {
               $ServiceProviderID = $serviceProvidersList[0]['ServiceProviderID'];
           
           }
           $_SESSION['session_spID'] = $ServiceProviderID;
           
           $Engineer        = $this->loadModel('Engineers');
           $EngineersList   = $Engineer->fetchAll($ServiceProviderID, 'Active');
           
           
           $ServiceProviderSkillset             = $this->loadModel('ServiceProviderSkillsSet');
           $AppointmentAllocationSlots          = $ServiceProviderSkillset->getAppointmentAllocationSlot($ServiceProviderID, true);

            
           
           $ServiceProvidersModel = $this->loadModel('ServiceProviders');
           $DiaryModel = $this->loadModel('Diary');
           $ServiceProviderRow    = $ServiceProvidersModel->fetchRow(array("ServiceProviderID"=>$ServiceProviderID));
           
           $timeDefPostcodeRow=array();
           $timeDefRow=$DiaryModel->getTimeDefRow($ServiceProviderID);
           foreach($timeDefRow as $d){
            $timeDefPostcodeRow[$d['DeferredTimeSlotID']]=$DiaryModel->getTimeDefPostcodeRow($d['DeferredTimeSlotID']);   
           }
          
//           echo"<pre>";
//           print_r($timeDefRow);
//           print_r($timeDefPostcodeRow);
//           echo"</pre>";
         $this->smarty->assign("timeDefRow", $timeDefRow);
         $this->smarty->assign("timeDefPostcodeRow", $timeDefPostcodeRow);
           
           if(!isset($ServiceProviderRow['DefaultTravelTime']))
           {
              $ServiceProviderRow['DefaultTravelTime'] = ''; 
           } 
           if(!isset($ServiceProviderRow['LockAppWindowTime']))
           {
              $ServiceProviderRow['LockAppWindowTime'] = '60'; 
           } 
           
           if(!isset($ServiceProviderRow['DefaultTravelSpeed']))
           {
              $ServiceProviderRow['DefaultTravelSpeed'] = ''; 
           }    
           if(!isset($ServiceProviderRow['BookingCapacityLimit']))
           {
              $ServiceProviderRow['BookingCapacityLimit'] = ''; 
           }    
           
           if(!isset($ServiceProviderRow['AutoChangeTimeSlotLabel']))
           {
              $ServiceProviderRow['AutoChangeTimeSlotLabel'] = 'No'; 
           } 
           
           if(!isset($ServiceProviderRow['RunViamenteToday']))
           {
              $ServiceProviderRow['RunViamenteToday'] = 'No'; 
           }
           if(!isset($ServiceProviderRow['LockAppWindow']))
           {
              $ServiceProviderRow['LockAppWindow'] = 'No'; 
           }
           if(!isset($ServiceProviderRow['KeepEngInPCArea']))
           {
              $ServiceProviderRow['KeepEngInPCArea'] = 'No'; 
           }
           
           
           
           if(strlen($ServiceProviderRow['PasswordProtectNextDayBookingsTime'])==8)
           {
               $ServiceProviderRow['PasswordProtectNextDayBookingsTime'] = substr($ServiceProviderRow['PasswordProtectNextDayBookingsTime'], 0, -3);
           }
           
           if(strlen($ServiceProviderRow['SendRouteMapsEmail'])==8)
           {
               $ServiceProviderRow['SendRouteMapsEmail'] = substr($ServiceProviderRow['SendRouteMapsEmail'], 0, -3);
           }
           
           $dataImportedToSB        = false;
           $sk_va_test              = false;
           $sk_sb_test              = false;
           
           if(isset($_SESSION['DiaryWizardSetup']) && $_SESSION['DiaryWizardSetup'])
           {
               $this->smarty->assign('DiaryWizardSetup', true);
               
               if($sPage==7 || $sPage==8)//Calling  PutDiaryWizardSelection  call. || Running Communication Test between Skyline and Service Base
               {
                   $APIAppointmentsModel             = $this->loadModel('APIAppointments');
                   $PutDiaryWizardSelectionResponse  = $APIAppointmentsModel->PutDiaryWizardSelection($ServiceProviderID);
                   
                   if(isset($PutDiaryWizardSelectionResponse['ResponseCode']) && $PutDiaryWizardSelectionResponse['ResponseCode'] == 'SC0001')
                   {
                       $dataImportedToSB = $sk_sb_test = true;
                   }
               }
               
               if($sPage==8)//Running Communication Test between Skyline and Viamente
               {
                   
                   $APIViamenteModel                 = $this->loadModel('APIViamente');
                   $ListEngineersResponse  = $APIViamenteModel->ListEngineers();
                   
                   if(isset($ListEngineersResponse['Message']) && $ListEngineersResponse['Message'])
                   {
                       
                   } 
                   else
                   {
                       $sk_va_test = true;
                   }
                   
                   
               }
               
               if($ServiceProviderRow['OnlineDiary']!='Active')
               {
                   $ServiceProviderRow['OnlineDiary'] = 'Active';
               }
               
           }
           else
           {
               $this->smarty->assign('DiaryWizardSetup', false);
           }
           
           
           
           
           
           $this->smarty->assign('dataImportedToSB', $dataImportedToSB);
           
           $this->smarty->assign('sk_sb_test', $sk_sb_test);
           $this->smarty->assign('sk_va_test', $sk_va_test);
           
           $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
           $this->smarty->assign('ServiceProviderID', $ServiceProviderID);
           $this->smarty->assign('DefaultTravelTime', $ServiceProviderRow['DefaultTravelTime']);
           $this->smarty->assign('LockAppWindowTime', $ServiceProviderRow['LockAppWindowTime']);
           $this->smarty->assign('DefaultTravelSpeed', $ServiceProviderRow['DefaultTravelSpeed']);
           $this->smarty->assign('BookingCapacityLimit', $ServiceProviderRow['BookingCapacityLimit']);
           $this->smarty->assign('KeepEngInPCArea', $ServiceProviderRow['KeepEngInPCArea']);
           $this->smarty->assign('AutoChangeTimeSlotLabel', $ServiceProviderRow['AutoChangeTimeSlotLabel']);
           $this->smarty->assign('RunViamenteToday', $ServiceProviderRow['RunViamenteToday']);
           $this->smarty->assign('LockAppWindow', $ServiceProviderRow['LockAppWindow']);
           $this->smarty->assign('SetupUniqueTimeSlotPostcodes', $ServiceProviderRow['SetupUniqueTimeSlotPostcodes']);
           $this->smarty->assign('UnlockingPassword', $ServiceProviderRow['UnlockingPassword']);
           $this->smarty->assign('PasswordProtectNextDayBookings', $ServiceProviderRow['PasswordProtectNextDayBookings']);
           $this->smarty->assign('PasswordProtectNextDayBookingsTime', $ServiceProviderRow['PasswordProtectNextDayBookingsTime']);
           $this->smarty->assign('DiaryAdministratorEmail', $ServiceProviderRow['DiaryAdministratorEmail']);
           $this->smarty->assign('engDefStart', $ServiceProviderRow['EngineerDefaultStartTime']);
           $this->smarty->assign('engDefEnd', $ServiceProviderRow['EngineerDefaultEndTime']);
           $this->smarty->assign('DeffTimeFrom', '');
           $this->smarty->assign('DeffTimeTo', '');
           $this->smarty->assign('DeffPostcode', '');
           
          
           
           $this->smarty->assign('CurrentDate', date("d/m/Y"));
           
           $PMTimeTo = '';
           $PMTimeFrom = '';
           $PMColour    =  '';
           $AMTimeFrom = '';
           $AMTimeTo = '';
           $AMColour = '';
           
           
           if(is_array($AppointmentAllocationSlots))
           {
               foreach($AppointmentAllocationSlots AS $aal)
               {
                   if($aal['Type']=="AM")
                   {
                       $AMTimeFrom = substr($aal['TimeFrom'], 0, 5);
                       $AMTimeTo   = substr($aal['TimeTo'], 0, 5); 
                       $AMColour   = $aal['Colour'];
                   } 
                   else  if($aal['Type']=="PM")
                   {
                       $PMTimeFrom = substr($aal['TimeFrom'], 0, 5); 
                       $PMTimeTo   = substr($aal['TimeTo'], 0, 5);
                       $PMColour   = $aal['Colour'];
                   }
               }    
           }    
           
           $this->smarty->assign('tInfo', $tInfo);
           $this->smarty->assign('sPage', $sPage);
           $this->smarty->assign('SuperAdmin', $SuperAdmin);
           
           $this->smarty->assign('PMTimeTo', $PMTimeTo);
           $this->smarty->assign('PMTimeFrom', $PMTimeFrom);
           $this->smarty->assign('PMColour', $PMColour);
           
           $this->smarty->assign('AMTimeFrom', $AMTimeFrom);
           $this->smarty->assign('AMTimeTo', $AMTimeTo);
           $this->smarty->assign('AMColour', $AMColour);
           $this->smarty->assign('cId', $cId);
           $this->smarty->assign('EngineersList', $EngineersList);
           $this->smarty->assign('ServiceProviderRow', $ServiceProviderRow);
            if($sPage==9)//Running Communication Test between Skyline and Viamente
               {
                  $diary=$this->loadModel('Diary');
                  $data=$diary->getSpGridData($ServiceProviderID);
                 
            $this->smarty->assign('data', $data);
            
               }
               if($sPage==10)//bank holiday
               {
                  $diary=$this->loadModel('Diary');
                  $data=$diary->getBankHolidays($ServiceProviderID,$cId);
                 
            $this->smarty->assign('data', $data);
            
               }
           
            $diary = $this->loadModel('Diary');
                             $dat=$diary->getSPAllocationType($ServiceProviderID);
                             $allocOnly=$diary->getDiaryType($ServiceProviderID);
        $this->smarty->assign('dat', $dat);
        $this->smarty->assign('allocOnly',$allocOnly);
           $this->smarty->display('diaryDefaults.tpl'); 



            return;
        
       
      }
      
      
      
      
      
      
      
      
      /**
     * Description
     * 
     * This method is used for to manuplate data of Service Provider Trade Accounts Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function spTradeAccountAction(  $args  ) {
          
            
          
          $functionAction           = isset($args[0])?$args[0]:false;
          $ServiceProviderID        = isset($args[1])?$args[1]:false;
          $selectedRowId            = isset($args[2])?$args[2]:false;
          
           
          $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));
                
          $accessErrorFlag = false;
          
          $this->page      = $this->messages->getPage('spTradeAccount', $this->lang);
          $this->smarty->assign('page', $this->page);
          
          
           
           if($this->user->ServiceProviderID) 
           {
               $ServiceProviderID = $this->user->ServiceProviderID;
               
           }
           


            switch( $functionAction) {

                case 'insert':

                        $datarow = array('ServiceProviderTradeAccountID'=>'', 'TradeAccount'=>'', 'Postcode'=>'', 'Monday'=>'', 'Tuesday'=>'', 'Wednesday'=>'', 'Thursday'=>'', 'Friday'=>'', 'Saturday'=>'', 'Sunday'=>'', 'Status'=>'Active');
                        

                        //Checking user permissons to display the page.
                        if($ServiceProviderID)
                        {
                            $accessErrorFlag = false;
                        }
                        else 
                        {  
                            $accessErrorFlag = true;
                        }

                        $this->smarty->assign('datarow', $datarow);
                        $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                        $this->smarty->assign('ServiceProviderID', $ServiceProviderID);


                        $htmlCode = $this->smarty->fetch('spTradeAccountForm.tpl');

                        echo $htmlCode;

                    break;


                case 'update':


                        $args['ServiceProviderTradeAccountID'] = $selectedRowId;
                        $model     = $this->loadModel('SPTradeAccount');

                        $datarow   = $model->fetchRow($args);


                        $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                       
                        $this->smarty->assign('datarow', $datarow);

                        //Checking user permissons to display the page.
                        if($ServiceProviderID)
                        {
                            $accessErrorFlag = false;
                        }
                        else 
                        {    
                            $accessErrorFlag = true;
                        }    

                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                        $this->smarty->assign('ServiceProviderID', $ServiceProviderID);


                        $htmlCode = $this->smarty->fetch('spTradeAccountForm.tpl');

                    echo $htmlCode;    

                    break;

                 default:

                     

                }  
         
      }
      
      
      
       /**
     * Description
     * 
     * This method is used for to display all appointments booked on Skyline diary.
     * 
     * @param array $args 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function wallboardAction(  $args  ) {
          
            
          
            if($this->user->SuperAdmin || $this->user->NetworkID)
            {

                   $aType  =  isset($args['aType'])?$args['aType']:'all';
                   $this->smarty->assign('aType', $aType);
                   
                   $dType  =  isset($args['dType'])?$args['dType']:'all';
                   $this->smarty->assign('dType', $dType);
                   
                   
                   $spID  =  isset($args['spID'])?$args['spID']:'-1';
                   $this->smarty->assign('spID', $spID);
                   
                   $dataSPID  =  isset($args['dataSPID'])?$args['dataSPID']:false;
                   $this->smarty->assign('dataSPID', $dataSPID);
                   
                   $openJobs  =  isset($args['openJobs'])?$args['openJobs']:false;
                   $this->smarty->assign('openJobs', $openJobs);
                   
                   $appsDate  =  isset($args['appsDate'])?$args['appsDate']:false;
                   $this->smarty->assign('appsDate', $appsDate);
                   
                

                   $page = $this->messages->getPage('diaryPerformance', $this->lang);
                   $this->smarty->assign('page', $page);
                   
                    $args['sType']        = isset($args['sType']) ? $args['sType'] : 0;
                    $this->smarty->assign('sType', $args['sType']);  
                   
                   
                    $SkylineModel = $this->loadModel('Skyline');
                   
                    if($this->user->SuperAdmin)
                    {    
                         $ServiceProvidersList =  $SkylineModel->getServiceProviders(null, true, $dType);
                    }
                    else if($this->user->NetworkID)
                    {
                         $this->smarty->assign('NetworkID', $this->user->NetworkID);
                         
                         $ServiceProvidersList  =  $SkylineModel->getNetworkServiceProviders($this->user->NetworkID, true, $dType);
                    }
                    
                   // $this->log("dddddddddddddd");
                   // $this->log($ServiceProvidersList);
                    
                    $this->smarty->assign('ServiceProvidersList', $ServiceProvidersList);

                   $dateRange = $dateRange2 = array();
                   
                   for($i=0;$i<7;$i++)
                   {
                       
                       if(date("N", strtotime ( $i.' day'))==7)//Sunday
                       {
                           $Sunday = $i;
                       }
                       
                       $dateRange[] =   strtoupper(date("D d M", strtotime ( $i.' day')));
                       $dateRange2[] =   date("Y-m-d", strtotime ( $i.' day'));
                   }
                   
                   $wallBoardResult  = $this->wallboardResultAction( $dateRange2, $aType, $spID, $dType);
                   
                   $data  = isset($wallBoardResult[0])?$wallBoardResult[0]:array();
                   $error = isset($wallBoardResult[1])?$wallBoardResult[1]:array();
                   
                   //$this->log($data);
                   
                   
                   $SundayFlag = false;
                   
                   $SundayData = (3*$Sunday)+3;
                   
                   foreach($data as $dt)
                   {
                       if($dt[$SundayData])
                       {
                           $SundayFlag = true;
                       }
                   }
                   
                   
//                    if($this->user->NetworkID)
//                    {    
//                            $dataCount = count($data);
//                            if($dataCount)
//                            {    $dataLength = count($data[0]);
//                                
//                                $data[$dataCount][0] = '';
//                                $data[$dataCount][1] = $page['Text']['total_capital'];
//                                $data[$dataCount][2] = '';
//                                
//                                $error[$dataCount][0] = $error[$dataCount][1] = 0;
//                                
//                                 for($j=3;$j<$dataLength;$j++)
//                                 {
//                                     $data[$dataCount][$j]  = 0;
//                                     $error[$dataCount][$j] = 0;
//                                 }
//
//                                 for($i=0;$i<$dataCount;$i++)
//                                 {
//                                     for($j=3;$j<$dataLength;$j++)
//                                     {
//                                         $data[$dataCount][$j] += $data[$i][$j];
//                                     }
//                                 }
//                            }
//
//                     }
                   
                   
                 //  $this->log('testttttt');  
                   if($aType!='all')//Removing service provider row if it has no data - starts here... 
                   {
                       $dataTemp  = $data;
                       $errorTemp = $error;
                       
                       $data = $error = array();
                       
                       for($i=0;$i<count($dataTemp);$i++)
                       {
                           $RemoveDataRowFlag = true;
                           
                           if($this->user->SuperAdmin)
                           {    
                                $j=3;
                           }
                           else
                           {
                               $j=2;
                           }
                           
                           for(;$j<count($dataTemp[$i]);$j++)
                           {
                               if($dataTemp[$i][$j])
                               {    
                                   $RemoveDataRowFlag = false;
                                   break;
                               }
                               
                           }
                           
                           if($RemoveDataRowFlag)
                           {
                               for($j=0;$j<count($errorTemp[$i]);$j++)
                                {
                                    if($errorTemp[$i][$j])
                                    {    
                                        $RemoveDataRowFlag = false;
                                        break;
                                    }

                                }
                           }
                           
                           if(!$RemoveDataRowFlag)
                           {
                               $data[]  = $dataTemp[$i];
                               $error[] = $errorTemp[$i];
                           }    
                           
                       }
                       
                       if($this->user->SuperAdmin)
                       {
                          $dataCount =  count($data);
                          if($dataCount>1)
                          {    
                            $data[$dataCount-1][1][0] = $page['Text']['total_service_provider'].': '.($dataCount-1);
                          }
                       }    
                       
                   }   //Removing service provider row if it has no data - ends here... 
                   
//                   $this->log($data);  
//                   $this->log($error);
//                   
//                   $this->log($dateRange);
//                   $this->log($dateRange2);
                   
                     
                   $this->smarty->assign('dateRange', $dateRange);
                   $this->smarty->assign('sqlDateRange', $dateRange2);
                   
                   
                   $this->smarty->assign('data', $data);
                   $this->smarty->assign('error', $error);
                    $this->log($data,"dataa_____________________");
                    $this->log($error,"dataa_____________________");
                  
                   $this->smarty->assign('Sunday', $Sunday);
                   $this->smarty->assign('SundayData', $SundayData);
                   $this->smarty->assign('SundayFlag', $SundayFlag);
                   
                   if($dataSPID)
                   {    
                       
                      
                        $JobModel       = $this->loadModel('Job');
                       
                            
                        $statsDataParam = array('ServiceProviderID'=>$dataSPID, 'NetworkID'=>$this->user->NetworkID, 'aType'=>$aType, 'dType'=>$dType);
                        
                        if($openJobs)
                        {
                            
                            $statsResult    = $JobModel->serviceProviderOpenJobsWallboardChartStats($statsDataParam);
                        }
                        else
                        {
                            $statsDataParam['AppointmentDate'] = $appsDate;
                            
                            $statsResult    = $JobModel->serviceProviderAppJobsWallboardChartStats($statsDataParam); 
                        }
                       
                        for($r=0;$r<count($statsResult);$r++)
                        {
                            $statsResult[$r][4] = $statsResult[$r][0];
                            $statsResult[$r][0] = $statsResult[$r][0]." (".$statsResult[$r][1].")";

                        }

                        $totalRows = 0;
                        $selectedStatusClient = '';
                        for($r=0;$r<count($statsResult);$r++)
                        {

                            if($args['sType'] && $args['sType']==$statsResult[$r][2])
                            {
                                $totalRows = $statsResult[$r][1];
                                $selectedStatusClient = " ".$statsResult[$r][4];
                                break;
                            }
                            else
                            {
                                $totalRows += $statsResult[$r][1];
                            }
                        }
                       
                       
                          
                        $this->smarty->assign('statsResult', $statsResult);
                        
                        $graph_title = $totalRows." ".(($appsDate)?$page['Text']['app_jobs']:$page['Text']['total_open_jobs']).$selectedStatusClient; 
                        $this->smarty->assign('graph_title', $graph_title);
                        
                   }
                   
                   $this->smarty->assign('SuperAdmin', ($this->user->SuperAdmin)?true:false);
                   
                   $this->smarty->display('diaryPerformance.tpl'); 

                   return;
            }
            else
            {
                 $this->redirect('index'); 
            }
        
      }
      
      
      
      /**
     * wallboardResultAction
     * This method is used for to get all appointments booked on Skyline diary
     
     * @param array  $dateRange
     * @param string $aType 
     * @param int $spID  
     * @param string $dType 
     * 
     * @return array
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     ********************************************/
      public function wallboardResultAction( $dateRange, $aType, $spID, $dType) { 
         
            $ServiceProvidersList = array();
            
            $SkylineModel = $this->loadModel('Skyline');
            $DiaryModel   = $this->loadModel('Diary');
            $UsersModel   = $this->loadModel('Users');
            $JobsModel    = $this->loadModel('Job');
            
            
            $page = $this->messages->getPage('diaryPerformance', $this->lang);
            $this->smarty->assign('page', $page);
            
            
            if($this->user->SuperAdmin)
            {    
                 $ServiceProvidersListTemp =  $SkylineModel->getServiceProviders(null, true, $dType);
            }
            else if($this->user->NetworkID)
            {
                 $ServiceProvidersListTemp  =  $SkylineModel->getNetworkServiceProviders($this->user->NetworkID, true, $dType);
            }
            
            
            if($spID!='-1')
            {
                for($spCnt=0;$spCnt<count($ServiceProvidersListTemp);$spCnt++)
                {
                    if($ServiceProvidersListTemp[$spCnt]['ServiceProviderID']==$spID)
                    {
                        $ServiceProvidersList[0] = $ServiceProvidersListTemp[$spCnt];
                        break;
                    }    
                }
            }
            else
            {
                $ServiceProvidersList = $ServiceProvidersListTemp;
            }
            
           // $this->log("ddeeeeee");
           // $this->log($ServiceProvidersList);

            $spCount = count($ServiceProvidersList); 

            $dataResult  = array();
            $errorResult = array();
            
            if($spCount)
            {    
                $dataResult[$spCount][0] = '';
                $dataResult[$spCount][1] = array($page['Text']['total_service_provider'].': '.$spCount, '');
                $dataResult[$spCount][2] = '';
            }
            
            for($i=0;$i<$spCount;$i++)
            {
                
                $diaryType = '';
                
                if($ServiceProvidersList[$i]['DiaryType']=="FullViamente")
                {
                    $diaryType = 'VIA';
                }
                else if($ServiceProvidersList[$i]['DiaryType']=="NoViamente")
                {
                    $diaryType = 'SA';
                }
                else if($ServiceProvidersList[$i]['DiaryType']=="AllocationOnly")
                {
                    $diaryType = 'AO';
                }
                
                $samsungDataResultTemp = array();
                
                if($this->user->SuperAdmin)
                {    
                    $dataResultTemp          =  $DiaryModel->getDiaryWallboardPerformance($ServiceProvidersList[$i]['ServiceProviderID'], false, $aType);
                  
                    $unreachedDataResultTemp =  $DiaryModel->getDiaryWallboardPerformance($ServiceProvidersList[$i]['ServiceProviderID'], false, $aType, true);
               
                
                    //Getting Samsung appointments..
                    $samsungDataResultTemp =  $DiaryModel->getDiaryWallboardPerformance($ServiceProvidersList[$i]['ServiceProviderID'], 2, $aType);
                    
                }
                else if($this->user->NetworkID)
                {
                     $dataResultTemp =  $DiaryModel->getDiaryWallboardPerformance($ServiceProvidersList[$i]['ServiceProviderID'], $this->user->NetworkID, $aType);
                     
                     
                     //Getting non Skyline Jobs - starts here..
                     $dataResultNSJTemp =  $DiaryModel->getDiaryWallboardPerformance($ServiceProvidersList[$i]['ServiceProviderID'], $this->user->NetworkID, $aType, false, true);
                     
//                     $this->log("test");
//                     $this->log($dataResultTemp);
//                     $this->log("--------------------------------");
//                     $this->log($dataResultNSJTemp);
                    
                     //
                     foreach($dataResultNSJTemp as $nsjKey=>$nsjData)
                     {
                         if(isset($dataResultTemp[$nsjKey]))
                         {
                             foreach($nsjData as $nsjInnerKey=>$nsjInnerData)
                             {
                                 if(isset($dataResultTemp[$nsjKey][$nsjInnerKey]))
                                 {
                                     $dataResultTemp[$nsjKey][$nsjInnerKey]['apCountType'] += $nsjInnerData['apCountType'];
                                 }
                                 else
                                 {
                                     $dataResultTemp[$nsjKey][$nsjInnerKey] = $nsjInnerData;
                                 }
                             }
                         }
                         else
                         {
                             
                           $dataResultTemp[$nsjKey] = $nsjData;
                         }
                     }    
                     //Getting non Skyline Jobs - ends here..
//                     
//                     $this->log("--------------------------------");
//                     $this->log($dataResultTemp);
//                     
                     
                     
                     $unreachedDataResultTemp =  $DiaryModel->getDiaryWallboardPerformance($ServiceProvidersList[$i]['ServiceProviderID'], $this->user->NetworkID, $aType, true);
                }
                
                
               
                
                
             //   $this->log("test");
             //   $this->log($dataResultTemp);
                
                if($diaryType=="AO")
                {
                    $ErrorDays = $DiaryModel->getErrorApp($ServiceProvidersList[$i]['ServiceProviderID'], 14, false, true);
                }
                else
                {    
                    $ErrorDays = $DiaryModel->getErrorApp($ServiceProvidersList[$i]['ServiceProviderID'], 14);
                }
                
               // $this->log("ssssss");
               // $this->log($ErrorDays);
                
                
                $ServiceProviderUser  =  $UsersModel->getServiceProviderUser($ServiceProvidersList[$i]['ServiceProviderID']);
            
                $dataResult[$i][0] = $ServiceProvidersList[$i]['ServiceProviderID'];
                $dataResult[$i][1] = array($ServiceProvidersList[$i]['CompanyName'], $ServiceProviderUser);
                
                if($this->user->SuperAdmin)
                {
                    $dataResult[$i][2] = $diaryType;
                }
                else
                {
                    if($dataResult[$spCount][2]=='')
                    {    
                        $dataResult[$spCount][2] = 0;
                    }
                    
                    $dataResult[$i][2] = $JobsModel->getServiceProviderOpenJobs($ServiceProvidersList[$i]['ServiceProviderID'], $this->user->NetworkID, $aType);//Open jobs...
                
                    $dataResult[$spCount][2] = $dataResult[$spCount][2]+$dataResult[$i][2];
                }
                
                
                
                
                $cnt = 3;
                for($d=0;$d<count($dateRange);$d++)
                {
                    $errorResult[$i][$d] = ''; 
                    $errorResult[$spCount][$d] = '';//Total Row Value..
                    
                    //Unreached appointments calculations...starts here..
                    $totalUnreachedAppointments = 0;
                    if(isset($unreachedDataResultTemp[$dateRange[$d]]))
                    {    
                        foreach($unreachedDataResultTemp[$dateRange[$d]] as $unreachedStats)
                        {
                             $totalUnreachedAppointments += $unreachedStats['apCountType'];
                             $uTime[]=$unreachedStats['uTime'];
                        }
                    }
                    //getting max unreached time
                    $utt=0;
                    if(isset($uTime)){
                    foreach($uTime as $ut){
                        
                        if($ut>$utt){$utt=$ut;}
                    }
                    }
                    //Unreached appointments calculations...ends here..
                    
                    if(isset($dataResultTemp[$dateRange[$d]]))
                    {    
                        $totalEngineers    = 0;
                        $totalAppointments = 0;
                        
                        
                        
                        foreach($dataResultTemp[$dateRange[$d]] as $stats)
                        {
                             $totalAppointments += $stats['apCountType'];
                             $totalEngineers    = $stats['totalEngineers'];
                        } 
                        
                        
                        
                        
                        
                        if(!isset($dataResult[$spCount][$cnt]))
                        {
                            $dataResult[$spCount][$cnt] = 0;
                        }
                        $dataResult[$spCount][$cnt] = $dataResult[$spCount][$cnt]+$totalAppointments;//Total Row Value..
                        
                        if($this->user->SuperAdmin)
                        {    
                            $dataResult[$i][$cnt++] = ($totalUnreachedAppointments)?$totalAppointments.' ('.$totalUnreachedAppointments.') ['.$utt.']': $totalAppointments;
                        }
                        else
                        {
                            $dataResult[$i][$cnt++] = $totalAppointments; 
                        }
                        
                        if($this->user->SuperAdmin) //Samsung appointments...starts here..
                        {
                            $samsungAppointments = 0;
                            
                            if(isset($samsungDataResultTemp[$dateRange[$d]]))
                            {
                                foreach($samsungDataResultTemp[$dateRange[$d]] as $samsungAppts)
                                {
                                     $samsungAppointments += $samsungAppts['apCountType'];
                                }
                            }
                            
                           $secondColumnValue  =  $samsungAppointments;
                        
                        } //Samsung appointments...ends here..
                        else
                        {
                            $secondColumnValue = 0;
                           
                        }
                        
                        if(!isset($dataResult[$spCount][$cnt]))
                        {
                            $dataResult[$spCount][$cnt] = 0;
                        }
                        $dataResult[$spCount][$cnt] = $dataResult[$spCount][$cnt]+$secondColumnValue;//Total Row Value..

                        $dataResult[$i][$cnt++] = $secondColumnValue;
                        
                        
                        
                        
                        
                        $dataResult[$spCount][$cnt] = '';//Total Row Value..
                        
                        
                        if($totalEngineers)
                        {    
                            $dataResult[$i][$cnt++] = number_format($totalAppointments/$totalEngineers, 1);
                        }
                        else
                        {
                            $dataResult[$i][$cnt++] = 0.0;
                        }
                        
                        if(is_array($ErrorDays))
                        {
                                foreach($ErrorDays AS $ed)
                                {    

        //                            $this->log(date("d/m/Y", strtotime($dateRange[$d])));
        //                            $this->log($ed['AppointmentDate']);
        //                            $this->log("----");

                                    if($ed['AppointmentDate']==date("d/m/Y", strtotime($dateRange[$d])))
                                    {        
                                        $errorResult[$i][$d] = 1;
                                        break;
                                    }
                                }
                        
                        }
                    }
                    else
                    {
                        
                        if(!isset($dataResult[$spCount][$cnt]))
                        {
                            $dataResult[$spCount][$cnt] = 0;
                        }
                        $dataResult[$spCount][$cnt] = $dataResult[$spCount][$cnt]+0;//Total Row Value..
                        
                        
                        if($this->user->SuperAdmin)
                        {    
                            $dataResult[$i][$cnt++] = ($totalUnreachedAppointments)?'0 ('.$totalUnreachedAppointments.')': 0;
                        }
                        else
                        {
                            $dataResult[$i][$cnt++] = 0;
                        }
                        
                        
                        
                        $secondColumnValue = 0;
                        
                        if(!isset($dataResult[$spCount][$cnt]))
                        {
                            $dataResult[$spCount][$cnt] = 0;
                        }

                        $dataResult[$spCount][$cnt] = $dataResult[$spCount][$cnt]+$secondColumnValue;//Total Row Value..

                        $dataResult[$i][$cnt++] = $secondColumnValue;
                        
                        
                        $dataResult[$spCount][$cnt] = '';//Total Row Value..
                        $dataResult[$i][$cnt++] = 0.0;
                    }
                }    
            }
            
//            $this->log("TopData");
//            $this->log($errorResult);
//            $this->log($dataResult);
            
            ksort($dataResult);
            ksort($errorResult);
            
//             $this->log("BottomData");
//             $this->log($errorResult);
//             $this->log($dataResult);
             //$errorResult[1][0] = 1;
//            
            
            return array($dataResult, $errorResult);
            
           

//            $output =  array (
//                     'sEcho' => 0,
//                     'iTotalRecords' => $spCount,
//                     'iTotalDisplayRecords' => $spCount,
//                     'aaData' => $dataResult );
//        
//           
//            echo json_encode( $output );
//         
     }
      
      
      
      
      /**
        * Description
        * 
        * This method is used for to map skills and postcode.
        * 
        * @param array $args
        * @return void  
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */
   
        public function skillsAreaMapAction( $args ) { 
            
             $method = isset($args[0])?$args[0]:false;
             
              $this->page    = $this->messages->getPage('skillsAreaMap', $this->lang);
              $this->smarty->assign('page', $this->page);
                        
             switch($method) 
             {
                    
                        case 'insert':
                         
                             $ServiceProviderID          = isset($args[1])?$args[1]:false;
                            
                             $ServiceProviderSkillsSet   =  $this->loadModel('ServiceProviderSkillsSet');
                                
                             $Skillset  =  $ServiceProviderSkillsSet->getNotAssignedSkillSet($ServiceProviderID);
                             
                             
                             
                             $datarow = array(
                                 
                                    'ServiceProviderSkillsetID'=>'', 
                                    'SkillsetID'=>'', 
                                    'ServiceProviderID'=>$ServiceProviderID, 
                                    'ServiceDuration'=>'', 
                                    'EngineersRequired'=>'', 
                                    'Status'=>'Active'
                                 
                                 );
                             
                             
                             $this->smarty->assign('datarow', $datarow);
                             $this->smarty->assign('Skillset', $Skillset);
                             $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                             
                            
                             $htmlCode = $this->smarty->fetch('serviceProviderSkillsSetForm.tpl');
                             
                             echo $htmlCode;  
                            
                            break;
                     
                     
                        case 'update':
				 
                             $ServiceProviderID                  = isset($args[1])?$args[1]:false;
		             $args['ServiceProviderSkillsetID']  = isset($args[2])?$args[2]:false;
                             
                             
                             
                             
                             $ServiceProviderSkillsSet   =  $this->loadModel('ServiceProviderSkillsSet');
                             
                             $datarow   =  $ServiceProviderSkillsSet->fetchRow($args);
                                
                             $Skillset  =  $ServiceProviderSkillsSet->getNotAssignedSkillSet($ServiceProviderID, $datarow['SkillsetID']);
                            
                            
                             
                             
                             $this->smarty->assign('datarow', $datarow);
                             $this->smarty->assign('Skillset', $Skillset);
                             $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                            
                             $htmlCode = $this->smarty->fetch('serviceProviderSkillsSetForm.tpl');
                             
                             echo $htmlCode;  
                            
                            break;
                       
                        default:
                        
                         
                            
                       


                        $Skyline = $this->loadModel('Skyline');

                        $serviceProvidersList = $Skyline->getServiceProviders();

                       

                           $SuperAdmin = true; 
                           $ServiceProviderID = ''; 
                           if($this->user->ServiceProviderID) 
                           {
                               $ServiceProviderID = $this->user->ServiceProviderID;
                               $SuperAdmin = false;
                           }
                           else if(isset($args['spID']))
                           {
                               $ServiceProviderID = $args['spID'];
                           }
                           else if(isset($_SESSION['session_spID']) && $_SESSION['session_spID'])
                           {
                                $ServiceProviderID = $_SESSION['session_spID'];
                           }
                           else if(count($serviceProvidersList))
                           {
                               $ServiceProviderID = $serviceProvidersList[0]['ServiceProviderID'];

                           }
                           $_SESSION['session_spID'] = $ServiceProviderID;
                           
                           
                           
                           $ServiceProviderEngineerID = 0;
                           if(isset($args['eID']))
                           {
                               $ServiceProviderEngineerID = $args['eID'];
                           }
                           


                           $ServiceProviderSkillset             = $this->loadModel('ServiceProviderSkillsSet');
                           $AppointmentAllocationSlots          = $ServiceProviderSkillset->getAppointmentAllocationSlot($ServiceProviderID, true);
                           
                           $SCDetails  =  $Skyline->getServiceCenterDetails($ServiceProviderID);
                           
                           if(isset($SCDetails['SetupUniqueTimeSlotPostcodes']) && $SCDetails['SetupUniqueTimeSlotPostcodes']=="Yes")
                           {
                                $SetupUniqueTimeSlotPostcodes = "Yes";
                           }   
                           else
                           {
                                $SetupUniqueTimeSlotPostcodes = "No";
                                
                                if(count($AppointmentAllocationSlots)>0)
                                {    
                                    $AppointmentAllocationSlotsTemp = $AppointmentAllocationSlots;
                                    $AppointmentAllocationSlots     = array();
                                    $AppointmentAllocationSlots[0]  = isset($AppointmentAllocationSlotsTemp[0])?$AppointmentAllocationSlotsTemp[0]:array();
                                    
                                    $AppointmentAllocationSlots[0]['Description'] = $this->page['Labels']['all_day'];
                                    
                                }
                                
                           }
                           
                          // $this->log($AppointmentAllocationSlots);
                           

                           
                           $AllocatedDate   = date("Y-m-d", strtotime('monday this week'));
                           $AllocatedToDate = '';
                           $FocusDayNo      = 1;
                           $PreSelectWeekNumber = 1;
                           
                           if(isset($args['d']) && strlen($args['d'])==10 && $args['d']>=date("Y-m-d", strtotime('monday this week')))
                           {
                               
//                                if (date('w', strtotime($args['d'])) == 1)
//                                {        
//                                    $AllocatedDate = $args['d'];
//                                }
//                                else
//                                {    
//                                    $AllocatedDate = date("Y-m-d", strtotime(
//                                            
//                                        'last monday',
//                                         strtotime($args['d'])
//                                            
//                                    ));
//                                }
                             
                              $datetime1 = new DateTime($AllocatedDate);
                              $datetime2 = new DateTime($args['d']);
                              $interval = $datetime1->diff($datetime2);
  
                                
                              $FocusDayNo = ($interval->days+1)%7; 
                              
                              $PreSelectWeekNumber = floor(($interval->days+1)/7) + 1;
                              
                           }
                           
                          
                           if($AllocatedDate!='')
                           {
                              $AllocatedToDate    =  date('d/m/Y', strtotime('+27 days, '.$AllocatedDate));
                              $AllocatedDateTemp  =  $AllocatedDate;
                              $AllocatedDate      =  date('d/m/Y', strtotime($AllocatedDateTemp));
                           }    
                           

                           $datarow  = array(
                               
                            "FocusDayNo"=>$FocusDayNo, "AllocatedDate"=>$AllocatedDate, "AllocatedToDate"=>$AllocatedToDate, "ServiceProviderEngineerID"=>$ServiceProviderEngineerID, "AppointmentAllocationSlotID"=>(isset($AppointmentAllocationSlots[0]['AppointmentAllocationSlotID']))?$AppointmentAllocationSlots[0]['AppointmentAllocationSlotID']:''
                               , "SetupType"=>"Unique", 
                                "ReplicateType"=>"FourWeeksOnly",
                                "ReplicateStatusFlag"=>'No',
                                "ReplicatePostcodeAllocation"=>'Yes' 
                           );
                           
                           

                           
                           
                            if(isset($args['ShowAll']))
                            {
                               $_SESSION['SessionShowAll'] = $args['ShowAll'];  
                            }

                            if(!isset($_SESSION['SessionShowAll']))
                            {
                                $_SESSION['SessionShowAll'] = 0;
                            }  
                            
                            $diary = $this->loadModel('Diary');
                            $dat=$diary->getSPAllocationType($ServiceProviderID);
                            $this->smarty->assign('dat', $dat);


                          $this->smarty->assign('AppointmentAllocationSlots', $AppointmentAllocationSlots);
                          $this->smarty->assign('datarow', $datarow);
                          $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
                          $this->smarty->assign('ServiceProviderID', $ServiceProviderID);
                          $this->smarty->assign('SuperAdmin', $SuperAdmin);
                          $this->smarty->assign('SetupUniqueTimeSlotPostcodes', $SetupUniqueTimeSlotPostcodes);
                          
                          $this->smarty->assign('ShowAll', $_SESSION['SessionShowAll']);
                          
                          
                          $this->smarty->assign('PreSelectWeekNumber', $PreSelectWeekNumber);
                          
                          for($i=1;$i<=4;$i++)
                          {
                                $weekButtons[$i]  = array(


                                    "WeekClassName"=>"weekButtons",
                                    "WeekName"=>$this->page['Labels']['week']." ".date("W", strtotime("+".(($i-1)*7)." day", strtotime('monday this week')))
                              );

                          }
                          
                          $weekButtons[$PreSelectWeekNumber]['WeekClassName'] = 'weekButtonsActive';
                          
                          
                          $this->smarty->assign('weekButtons', $weekButtons);
                          
                           
                             $allocOnly=$diary->getDiaryType($ServiceProviderID);
       
                            $this->smarty->assign('allocOnly',$allocOnly);
                            if($dat=="GridMapping"){
                                $this->smarty->display('diary/defaultsGeoCell.tpl'); 
                                break;
                            }else{
                          $this->smarty->display('skillsAreaMap.tpl');
                            }

            }  

            return;
        
       
      }
      
      
      
      
    
      public function addDeferredTimeAction($args){
          $spid=$args['spid'];
           $diary=$this->loadModel('Diary');
           $diary->addDeferredTime($spid);
          
          
          
      }
      
      
      public function removeVisualEndDayAction($args){
        print_r($_POST);
        $diary=$this->loadModel('Diary');
        $diary->removeVisualEndDay($_POST['d'],$_POST['m'],$_POST['s']);
      }
      /* New Function added by Raju for TBL 432 code Starts Here */
      public function getFinalizedDayDetailsAction($args)
      {
          $diary=$this->loadModel('Diary');
          $finalizedDays = $diary->getFinalizedDayDetails($this->user->ServiceProviderID, $_POST['fDate']);
          echo $finalizedDays['cnt'];
      }
      
      public function getFinalisedDayHistoryPopupAction($args)
      {
          $date = substr($args[0],6,4)."-".substr($args[0],3,2)."-".substr($args[0],0,2);
          $this->page = $this->messages->getPage('diaryDefaults', $this->lang);
          $this->smarty->assign('page', $this->page);
          $this->smarty->assign('form_legend', $this->page['Text']['finalisedday_history_page_legend']);
          $this->smarty->assign('rDate', $date);
          $htmlCode = $this->smarty->fetch('finalizeDaysHistory.tpl');
          echo $htmlCode;
      }
      
      public function getFinalisedDayHistoryDetailsAction($args)
      {
          $diary=$this->loadModel('Diary');
          $finalizedDaysHistory = $diary->getFinalisedDayHistoryDetails($this->user->ServiceProviderID, $args[0]);
          echo json_encode($finalizedDaysHistory);
      }
    /* New Function added by Raju for TBL 432 code Ends Here */
    /* New Function added by Raju for TBL 317 code Starts Here */
    public function showBankHolidayCardAction($args) {
        $this->smarty->assign('cId', $_POST['cId']);
        echo $this->smarty->fetch('bankHolidayCard.tpl');
    }
    public function insertBankHolidayAction($args) {
        $diary=$this->loadModel('Diary');
        $diary->insertBankHoliday($this->user->ServiceProviderID,$_POST);
        echo json_encode(array(
            "status"    =>  "OK",
            "message"   =>  "Bank Holiday Data Inserted Successfully"));
    }
    /* New Function added by Raju for TBL 317 code Ends Here */
}
?>
