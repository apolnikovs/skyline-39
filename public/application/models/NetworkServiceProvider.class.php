<?php

/**
 * NetworkServiceProvider.class.php
 * 
 * Model with routines to access network_service_provider table in the Skyline
 * database
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.00
 * 
 * Changes
 * Date        Version Author                Reason
 * 03/08/2012  1.00    Andrew J. Williams    Initial Version
 * 22/08/2012  1.01    Andrew J. Williams    added fetchWhere method 
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class NetworkServiceProvider extends CustomModel {
    public $debug = false;
    private $conn;                                                              /* For database connection */
    private $table = "network_service_provider";                                /* Table Name */
    private $dbColumns = array(
                               'NetworkServiceProviderID',
                               'NetworkID',
                               'ServiceProviderID',
                               'AccountNo',
                               'SageSalesAccountNo',
                               'SagePurchaseAccountNo',
                               'TrackingUsername',
                               'TrackingPassword',
                               'WarrantyUsername',
                               'WarrantyPassword',
                               'WarrantyAccountNumber',
                               'Area',
                               'WalkinJobsUpload',
                               'CompanyCode',
                               'CreatedDate',
                               'EndDate',
                               'Status',
                               'ModifiedUserID',
                               'ModifiedDate',
                               'SamsungDownloadEnabled'  
                              );
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
       

    }
    
    /**
     * fetch
     * 
     * Fetch data from the network_service_provider tables based on arguments 
     * passed
     * 
     * @param $args     Associative Array of fields and values to be searched for
     * 
     * @return          Matching results
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function fetch($args) {
        $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        return  $output;
        
    }


    /**
     * fetchWhere
     * 
     * Return data based on a passed sql query where.
     * 
     * @param $where    WHERE clause for query
     * 
     * @return          Matching results
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function fetchWhere($where) {
        $sql = "
                SELECT
			*
		FROM
			$this->table
		WHERE
                        $where
               ";
    
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* ServiceProviderID exists so return Service Provider Details */
        } else {
            return(null);                                                       /* Not found return null */
        }
        
    }
}
?>
